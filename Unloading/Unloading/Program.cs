﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Configuration;
using static Alma.NetWrappers.CutPartFactory;
using Drafter;
using ActCut;
using Agi34;
using System.Xml.Linq;
using System.Windows.Forms;

namespace Unloading
{
    class Program
    {
        [DllImport("DprHeader", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode, EntryPoint = "DPRH_GetVarString")]
        public static extern long DPRH_GetVarString(string varName, string value);
        [DllImport("cCutPartFactoryFull", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateCpfMachiningToAllProfilesFromP01@8")]
        public static extern void AssociateCpfMachiningToAllProfilesFromP01(P01ImporterPtr importer, Machining cpf_machining);
        [DllImport("cCutPartFactoryFull", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_P01ImporterImport@4")]
        public static extern void P01ImporterImport(P01ImporterPtr importer);
        [DllImport("cCutPartFactoryFull", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode, EntryPoint = "_CPF_NewP01Importer@4")]
        public static extern P01ImporterPtr NewP01Importer(string filename);
        [DllImport("cCutPartFactoryFull", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_IsOnProfile@32")]
        public static extern int IsOnProfile(PartFactoryPtr part_factory_ptr, double x, double y, double distance, bool include_text);
        [DllImport("cCutPartFactoryFull", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewP01PartFactory@4")]
        public static extern PartFactoryPtr NewP01PartFactory(P01ImporterPtr importer);

        [DllImport("Drafter.dll", CallingConvention = CallingConvention.StdCall)]
        public static extern void AP_initDrafter(int apFile);

        [DllImport("Drafter.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode, EntryPoint = "AP_readFile")]
        public static extern long AP_readFile(string aFile, long fileType, out long error);

        //[DllImport("aph", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode)]
        [DllImport("aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode, EntryPoint = "APH_getKeyPath")]
        public static extern int APH_getKeyPath(string key, string path, int maxLength);

        [DllImport("aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode, EntryPoint = "APH_getBinPath")]
        public static extern int APH_getBinPath(string path, int maxLength);

        [DllImport("aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Unicode , EntryPoint = "APH_getRscPath")]
        public static extern int APH_getRscPath(string path, int maxLength);

        const int utilStrMaxLen = 512;

        static IApplication drafterApp = new Drafter.Application();
        static string logFolder = drafterApp.GetKeyPath("log");
        static LogFile logFile = new LogFile(logFolder + "Log_MireResult.txt");
        [STAThread]

        static void Main(string[] args)
        {
            string toto = APH_getRscPath_();

            var executablePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            Configuration config = ConfigurationManager.OpenExeConfiguration(executablePath);
            Console.Clear();
            if (args.Length == 1 && args[0].ToLower() == "-redo")
            {
                if (config.AppSettings.Settings["agiFileName"] != null && config.AppSettings.Settings["nestKey"] != null)
                    args = new string[] { config.AppSettings.Settings["agiFileName"].Value, config.AppSettings.Settings["nestKey"].Value };
            }

            if (args.Length != 2)
            {
                Console.WriteLine("Unloading.exe AgiFullPath KeyNestNumber");
                Console.WriteLine("Unloading.exe \"c:\\alma\\data\\Laser\\Rin\\123.agi\" \"KEY_NEST_1\"");
                Console.WriteLine("Unloading.exe -redo");
                Console.WriteLine("Appuyez une touche pour continuer . . .");
                Console.ReadKey(true);
                return;
            }

            List<string> errorCfgFile = new List<string>();

            if (!File.Exists(args[0]))
            {
                errorCfgFile.Add("Mire.exe \"" + args[0] + "\" \"" + args[1] + "\"");
                errorCfgFile.Add(args[0] + " fichier introuvable");
                ViewLog(errorCfgFile);
                return;
            }

            //  args must be agiFileName NestKey or -redo
            //  "C:\alma\Actcut39\Alma_NTX\imb\agi\512.agi" "KEY_NEST_1"
            if (args.Count() != 2)
                return;

            string agiFileName = args[0];
            string nestKey = args[1];

            config = ConfigurationManager.OpenExeConfiguration(executablePath);
            if (config.AppSettings.Settings["agiFileName"] != null)
                config.AppSettings.Settings["agiFileName"].Value = agiFileName;
            else
                config.AppSettings.Settings.Add("agiFileName", agiFileName);

            if (config.AppSettings.Settings["nestKey"] != null)
                config.AppSettings.Settings["nestKey"].Value = nestKey;
            else
                config.AppSettings.Settings.Add("nestKey", nestKey);

            config.Save(ConfigurationSaveMode.Full, true);

            Paletisation(agiFileName, nestKey);
        }
        private static void Paletisation(string agiFileName, string nestKey = "")
        {
            string dirName = Path.GetDirectoryName(agiFileName);
            string fileName = Path.GetFileName(agiFileName);
            string ext = Path.GetExtension(agiFileName);
            string arg = string.Empty;
            string destSvg = string.Empty;
            string outputfile = String.Empty;
            string cmd = String.Empty;

            AP_initDrafter(0);
            if (!string.IsNullOrEmpty(nestKey))
                arg = "NESTING|" + agiFileName + "|" + nestKey;
            else
                arg = "RIN|" + agiFileName;

            if (APH_getKeyPath_("svg") != "")
                destSvg = drafterApp.GetKeyPath("svg");
            else
                destSvg = drafterApp.GetKeyPath("rin");
            outputfile = destSvg + fileName + ".xml";
            cmd = "\"" + drafterApp.GetBinPath() + "OfflineUnloadingMgr.exe\" " +
                  "\"" + drafterApp.GetRscPath() + "OLUMachine-STX.xml\" " +
                  "\"" + arg + "\" " +
                  "\"" + outputfile + "\"";
            Clipboard.SetText(cmd);
            var process = Process.Start(cmd);
            process.WaitForExit();

            ParseXmlUnload(outputfile);

            int nbPart = 0;
            var result = AP_readFile("", 2, out long error);

            throw new NotImplementedException();
        }

        private static void ParseXmlUnload(string filename)
        {
            var xml = XDocument.Load(filename);

            throw new NotImplementedException();
        }

        private static void ViewLog(List<string> errorCfgFile)
        {
            string tmpFile = Path.GetTempFileName();
            using (StreamWriter sw = new StreamWriter(tmpFile))
            {
                foreach (string line in errorCfgFile)
                {
                    sw.WriteLine(line);
                }
            }
            Process.Start("notepad.exe", tmpFile);
        }

        static string APH_getBinPath_()
        {
            string s = new string(' ', utilStrMaxLen);
            if (APH_getBinPath(s, utilStrMaxLen) != 0)
                return ResizeNullTerminatedStringNoTrim(s);
            else
                return string.Empty;
        }

        static string APH_getRscPath_()
        {
            string s = new string(' ', utilStrMaxLen);
            if (APH_getRscPath(s, utilStrMaxLen) != 0)
                return ResizeNullTerminatedStringNoTrim(s);
            else
                return string.Empty;
        }

        static string APH_getKeyPath_(string key)
        {
            string s = new string(' ', utilStrMaxLen);
            if (APH_getKeyPath(key, s, utilStrMaxLen) != 0)
                return ResizeNullTerminatedStringNoTrim(s);
            else
                return string.Empty;
        }

        static string ResizeNullTerminatedStringNoTrim(string str)
        {
            str = str.TrimEnd('\0').TrimEnd();
            return str;
        }
    }
}
