﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DefragLST.Class
{
    public class LstTab
    {
        readonly List<string> dataList;
        public string TabName;
        public int startPropertyIdx;
        public int endPropertyIdx;
        public int nbProperty;

        public int startDataIdx;
        public int endDataIdx;
        public int nbData;
        public List<Tuple<string, string, string, string>> row = new List<Tuple<string, string, string, string>>();
        public List<List<string>> col = new List<List<string>>();
        public List<List<string>> dataRow = new List<List<string>>();
        public List<Tuple<string, List<string>>> cnFile = new List<Tuple<string, List<string>>>();
        public DataTable dataTable = new DataTable();

        public LstTab(List<string> Data)
        {
            dataList = new List<string>(Data);
            TabName = dataList[0];

            startPropertyIdx = dataList.FindIndex(p => p.StartsWith("ZA"));
            nbProperty = int.Parse(dataList[startPropertyIdx].Split(',').Last());
            endPropertyIdx = startPropertyIdx + nbProperty;

            for (int i = startPropertyIdx + 1; i <= endPropertyIdx; i++)
            {
                //  create row
                var spl = dataList[i].Split(',');
                row.Add(new Tuple<string, string, string, string>(spl[3].TrimStart(), spl[7], spl[9], spl[10]));
            }

            startDataIdx = endPropertyIdx + 2;
            if (startDataIdx < dataList.Count())
            {
                nbData = int.Parse(dataList[startDataIdx].Split(',').Last());
                endDataIdx = dataList.Count - 1;

                string tmpStr = "";
                int step = 0;

                if (dataList[0] == "BEGIN_PROGRAMM")
                {
                    List<string> prg = new List<string>();
                    string spName = "";
                    bool propertyRead = false;
                    bool endPropertyRead = false;
                    //  program data
                    for (int i = startDataIdx + 1; i < endDataIdx ; i++)
                    {
                        tmpStr = dataList[i];
                        if (tmpStr != "STOP_TEXT")
                            while (dataList[i + 1].StartsWith("* ") | dataList[i + 1].StartsWith("- "))
                            {
                                tmpStr += dataList[i + 1].TrimStart('*').TrimStart('-').TrimStart(' ');
                                i++;
                            }
                        if (tmpStr != "START_TEXT")
                        {
                            List<string> data1 = new List<string>();
                            if (!propertyRead)
                            {
                                tmpStr = tmpStr.Substring(3);
                                for (int i1 = 0; i1 < nbProperty; i1++)
                                {
                                    string value = "";
                                    if (row[i1].Item4 == "Z")
                                    {
                                        int virgule = tmpStr.IndexOf(',');
                                        if (virgule == -1)
                                        {
                                            value = tmpStr;
                                            tmpStr = "";
                                        }
                                        else
                                        {
                                            value = tmpStr.Substring(0, virgule);
                                            tmpStr = tmpStr.Substring(virgule + 1);
                                        }
                                    }
                                    else
                                    {
                                        int quote = -1;
                                        if (tmpStr[0] == '\'')
                                        {
                                            quote = tmpStr.IndexOf('\'', 1);
                                            value = tmpStr.Substring(1, quote - 1);
                                            try
                                            {
                                                tmpStr = tmpStr.Substring(quote + 2);
                                            }
                                            catch
                                            {
                                                tmpStr = "";
                                            }
                                        }
                                        else
                                        {
                                            int a = 0;
                                            quote = tmpStr.IndexOf(',', 0);
                                            if (quote > -1)
                                            {
                                                value = tmpStr.Substring(0, quote);
                                                tmpStr = tmpStr.Substring(quote + 1);
                                            }
                                            else
                                            {
                                                value = tmpStr;
                                                tmpStr = "";
                                            }
                                        }

                                    }
                                    data1.Add(value);
                                }
                                spName = data1[0];
                                col.Add(data1);
                                propertyRead = true;
                                prg.Clear();
                            }
                            else
                            {
                                if (dataList[i] == "STOP_TEXT")
                                    endPropertyRead = true;
                                else
                                    prg.Add(dataList[i]);

                                if (endPropertyRead)
                                {
                                    cnFile.Add(new Tuple<string, List<string>>(spName, new List<string>(prg)));
                                    propertyRead = false;
                                    endPropertyRead = false;
                                    //step++;
                                    //if (step == nbData)
                                    //    break;
                                }
                            }
                        }
                    }

                    dataRow =
                        col.SelectMany(inner => inner.Select((item, index) => new { item, index }))
                        .GroupBy(i => i.index, i => i.item)
                        .Select(g => g.ToList())
                        .ToList();

                    var dataRow1 =
                        col.Select(g => g.ToArray()).ToList();

                    dataTable = ConvertListToDataTable(dataRow1);
                }
                else
                {
                    //  standard data
                    for (int i = startDataIdx + 1; i <= endDataIdx; i++)
                    {
                        tmpStr = dataList[i];
                        while (dataList[i + 1].StartsWith("* ") | dataList[i + 1].StartsWith("- "))
                        {
                            tmpStr += dataList[i + 1].TrimStart('*').TrimStart('-').TrimStart(' ');
                            i++;
                        }

                        tmpStr = tmpStr.Substring(3);
                        List<string> data1 = new List<string>();

                        for (int i1 = 0; i1 < nbProperty; i1++)
                        {
                            string value = "";
                            if (row[i1].Item4 == "Z")
                            {
                                int virgule = tmpStr.IndexOf(',');
                                if (virgule == -1)
                                {
                                    value = tmpStr;
                                    tmpStr = "";
                                }
                                else
                                {
                                    value = tmpStr.Substring(0, virgule);
                                    tmpStr = tmpStr.Substring(virgule + 1);
                                }
                            }
                            else
                            {
                                int quote = -1;
                                if (tmpStr[0] == '\'')
                                {
                                    quote = tmpStr.IndexOf('\'', 1);
                                    value = tmpStr.Substring(1, quote - 1);
                                    try
                                    {
                                        tmpStr = tmpStr.Substring(quote + 2);
                                    }
                                    catch
                                    {
                                        tmpStr = "";
                                    }
                                }
                                else
                                {
                                    int a = 0;
                                    quote = tmpStr.IndexOf(',', 0);
                                    if (quote > -1)
                                    {
                                        value = tmpStr.Substring(0, quote);
                                        tmpStr = tmpStr.Substring(quote + 1);
                                    }
                                    else
                                    {
                                        value = tmpStr;
                                        tmpStr = "";
                                    }
                                }

                            }
                            data1.Add(value);
                        }

                        col.Add(data1);

                        step++;
                        if (step == nbData)
                            break;
                    }
                    dataRow =
                        col.SelectMany(inner => inner.Select((item, index) => new { item, index }))
                        .GroupBy(i => i.index, i => i.item)
                        .Select(g => g.ToList())
                        .ToList();

                    var dataRow1 =
                        col.Select(g => g.ToArray()).ToList();

                    dataTable = ConvertListToDataTable(dataRow1);
                }
            }
        }
        DataTable ConvertListToDataTable(List<string[]> list)
        {
            // New table.
            DataTable table = new DataTable();

            // Get max columns.
            int columns = 0;
            foreach (var array in list)
            {
                if (array.Length > columns)
                {
                    columns = array.Length;
                }
            }

            // Add columns.
            for (int i = 0; i < columns; i++)
            {
                table.Columns.Add();
            }

            // Add rows.
            foreach (var array in list)
            {
                table.Rows.Add(array);
            }

            return table;
        }
    }

}