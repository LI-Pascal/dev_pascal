﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;

namespace DefragLST
{
    public partial class Form1 : Form
    {
        [DllImport("user32.dll")]
        private static extern IntPtr PostMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);

        readonly List<string> extensionList = new List<string> { ".lst", ".ltt", ".tab" };
        List<Class.LstTab> lstTab = new List<Class.LstTab>();
        int findPos;

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (textBox1.Visible == true)
            {
                //  only with nc file visible
                if (msg.HWnd != textBox1.Handle)
                {
                    //  send key to search textbox (else space key)
                    if (keyData != Keys.Space)
                    {
                        if (keyData != Keys.Back)
                        {
                            if (textBox1.Text == Properties.Strings.Text_Search)
                                textBox1.Text = "";
                        }
                        else
                        {
                            if (textBox1.Text.Length == 1)
                                textBox1.Text = Properties.Strings.Text_Search;
                        }
                    }
                    PostMessage(textBox1.Handle, msg.Msg, msg.WParam, msg.LParam);
                    return true;
                }
                else
                {
                    //  if search textbox has handle
                    if (keyData == Keys.Back)
                        if (textBox1.Text.Length == 1)
                        {
                            textBox1.Text = Properties.Strings.Text_Search;
                            return true;
                        }
                        else
                            return false;

                    if (textBox1.Text == Properties.Strings.Text_Search)
                        textBox1.Text = "";
                }
                if (keyData == Keys.Escape)
                    textBox1.Text = Properties.Strings.Text_Search;
                if (keyData == Keys.F3)
                    next.PerformClick();
                if (keyData == (Keys.F3 | Keys.Shift))
                    previous.PerformClick();
                return false;
            }
            else
                return base.ProcessCmdKey(ref msg, keyData);
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBox1.Text = Properties.Strings.Text_Search;
            WordCount.Text = "";
            UpdateCaptionLocalizable();
        }

        private void dataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            // Vertical text from column 0, or adjust below, if first column(s) to be skipped
            if (e.RowIndex == -1 && e.ColumnIndex >= 0)
            {
                e.PaintBackground(e.CellBounds, true);
                e.Graphics.TranslateTransform(e.CellBounds.Left, e.CellBounds.Bottom);
                e.Graphics.RotateTransform(270);
                e.Graphics.DrawString(e.FormattedValue.ToString(), e.CellStyle.Font, Brushes.Black, 5, 5);
                e.Graphics.ResetTransform();
                e.Handled = true;
            }
        }

        private void UpdateCaptionLocalizable()
        {
            button_open.Text = Properties.Strings.Button_Open;
            button_Save.Text = Properties.Strings.Button_Save;
            button_Edit.Text = Properties.Strings.Button_Edit;
            button_close.Text = Properties.Strings.Button_Close;
            button_Export.Text = Properties.Strings.Button_Export;
            label_Row.Text = Properties.Strings.Label_Row;
            label_Column.Text = Properties.Strings.Label_Column;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
                if (CheckExtension(openFileDialog.FileName))
                    DefragmenteLst(openFileDialog.FileName);
        }

        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
        }

        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            var files = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (files.Length == 1)
                if (CheckExtension(files.First()))
                    DefragmenteLst(files.First());
        }

        private bool CheckExtension(string fileToCheck)
        {
            return extensionList.SingleOrDefault(p => p == Path.GetExtension(fileToCheck).ToLower()) != null;
        }

        private void DefragmenteLst(string lstFile)
        {
            tabList.Nodes.Clear();
            List<string> cnFile = File.ReadAllLines(lstFile).ToList();
            int startIndex = -1;
            int index = -1;
            List<string> bloc = null;
            lstTab = new List<Class.LstTab>();

            foreach (string ligne in cnFile)
            {
                if (ligne.StartsWith("BEGIN_"))
                {
                    if (bloc != null)
                        lstTab.Add(new Class.LstTab(bloc));
                    startIndex = index;
                    bloc = new List<string>();
                }

                if (ligne.StartsWith("ENDE_"))
                    for (int i = startIndex + 1; i <= index; i++)
                        bloc.Add(cnFile[i]);
                index++;
            }

            if (bloc != null)
                lstTab.Add(new Class.LstTab(bloc));

            if (lstTab != null)
            {
                foreach (Class.LstTab tab in lstTab)
                    tabList.Nodes.Add(tab.TabName);
                tabList.SelectedNode = tabList.Nodes[0];
            }
        }

        private void tabList_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //  load data in dataGridView
            dataGridView1.Columns.Clear();
            dataGridView1.ColumnHeadersVisible = false;

            var selectedTab = lstTab.Where(p => p.TabName == tabList.SelectedNode.Text).FirstOrDefault();
            if (selectedTab.TabName == "BEGIN_PROGRAMM")
                SetCnFileControlVisible(true);
            else
                SetCnFileControlVisible(false);

            int colNumber = 0;
            string item3 = string.Empty;
            foreach (Tuple<string, string, string, string> row1 in selectedTab.row)
            {
                //  création des colonnes et des headers
                item3 = row1.Item3.Trim('\'');
                if (!string.IsNullOrWhiteSpace(item3))
                    item3 = "(" + item3 + ")";

                int columnNmbr = dataGridView1.Columns.Add(colNumber.ToString(), row1.Item1 + "\n" + row1.Item2.Replace("'", "").TrimEnd() + "\n" + item3);
                //dataGridView1.Columns[columnNmbr].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
                colNumber++;
            }

            colNumber = 0;
            foreach (List<string> rangee in selectedTab.col)
            {
                //  creation de lignes et des headers
                DataGridViewRow row = new DataGridViewRow();
                var tito = rangee.ToArray();
                int nmbr = dataGridView1.Rows.Add(tito);
                dataGridView1.Rows[nmbr].HeaderCell.Value = nmbr.ToString();
            }
            dataGridView1.ColumnHeadersVisible = true;
            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            }
            label_Row.Text = selectedTab.nbData.ToString();
            label_Column.Text = selectedTab.nbProperty.ToString();
        }

        private void SetCnFileControlVisible(bool visibility)
        {
            textBox1.Visible = visibility;
            WordCount.Visible = visibility;
            splitContainer2.Panel2Collapsed = !visibility;
        }

        private void FillCnFile(int spNumber)
        {
            if (tabList.SelectedNode.Text == "BEGIN_PROGRAMM")
            {
                var ProgrammTab = lstTab.Where(p => p.TabName == "BEGIN_PROGRAMM").FirstOrDefault();
                cnFile.Text = string.Join("\n", ProgrammTab.cnFile[spNumber].Item2);
            }
        }

        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = Properties.Strings.Text_Search;
            FillCnFile(e.RowIndex);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            HighlightWords();
        }

        private void HighlightWords()
        {
            int count = 0;
            cnFile.SelectionStart = 0;
            cnFile.SelectionLength = cnFile.Text.Length;
            cnFile.SelectionBackColor = Color.White;

            string word = textBox1.Text;
            int startIndex = 0;
            while (startIndex < cnFile.TextLength)
            {

                int wordStartIndex = cnFile.Find(word, startIndex, RichTextBoxFinds.None);
                if (wordStartIndex != -1)
                {
                    count++;
                    cnFile.SelectionStart = wordStartIndex;
                    cnFile.SelectionLength = word.Length;
                    cnFile.SelectionBackColor = Color.Yellow;
                }
                else
                    break;
                startIndex = wordStartIndex + word.Length;
            }
            if (count > 0)
                WordCount.Text = count.ToString() + " found";
            else
                WordCount.Text = "";
            if (count > 1)
                next.Enabled = true;

            previous.Visible = count > 0;
            next.Visible = count > 0;
            previous.Enabled = false;
            findPos = 0;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (textBox1.Text == Properties.Strings.Text_Search)
                textBox1.Text = "";
        }

        private void next_Click(object sender, EventArgs e)
        {
            try
            {
                string s = textBox1.Text;
                cnFile.Focus();
                findPos = cnFile.Find(s, findPos, RichTextBoxFinds.None);
                cnFile.Select(findPos, s.Length);
                RichTextBox tmpRichTextBox = new RichTextBox();
                tmpRichTextBox.Text = cnFile.Text;
                findPos += textBox1.Text.Length + 1;
                previous.Enabled = true;
                if (tmpRichTextBox.Find(s, findPos, RichTextBoxFinds.None) == -1)
                    next.Enabled = false;
            }
            catch
            {
                findPos = cnFile.Text.Length;
                next.Enabled = false;
            }
        }

        private void previous_Click(object sender, EventArgs e)
        {
            try
            {
                string s = textBox1.Text;
                cnFile.Focus();
                findPos = cnFile.Find(s, 0, findPos - s.Length, RichTextBoxFinds.Reverse);
                cnFile.Select(findPos, s.Length);
                RichTextBox tmpRichTextBox = new RichTextBox();
                tmpRichTextBox.Text = cnFile.Text;
                findPos += textBox1.Text.Length;
                if (tmpRichTextBox.Find(s, 0, findPos - s.Length, RichTextBoxFinds.Reverse) == -1)
                    previous.Enabled = false;

                next.Enabled = true;
            }
            catch
            {
                findPos = 0;
                previous.Enabled = false;
            }
        }        
    }
}
