using System.Runtime.InteropServices;
#pragma warning disable 169 // Suppress warning message for unused variable
#pragma warning disable 649 // Field is never assigned to, and will always have its default value null

namespace Alma.NetWrappers
{
    public partial class Topo2dTools
    {
        public struct ListEdgeIteratorPtr { internal System.IntPtr __Ptr; }
        public static bool IsNull(ListEdgeIteratorPtr p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref ListEdgeIteratorPtr p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref ListEdgeIteratorPtr p, System.IntPtr value) { p.__Ptr = value; }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsDeleteListEdgeIterator@4")]
        public static extern void DeleteListEdgeIterator(ListEdgeIteratorPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsListEdgeIteratorHasNext@4")]
        public static extern bool ListEdgeIteratorHasNext(ListEdgeIteratorPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsListEdgeIteratorNext@4")]
        public static extern Topo2d.Edge ListEdgeIteratorNext(ListEdgeIteratorPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsGetMinimalHeightAngle@4")]
        public static extern double GetMinimalHeightAngle(Topo2d.Profiles profiles);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsGetPointAtDistanceFromVertex@20")]
        public static extern bool GetPointAtDistanceFromVertex(Topo2d.Vertex vertex, double curvilinear_length, out double point_found_x, out double point_found_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsGetLinearEdgesOnConvexHull@4")]
        public static extern ListEdgeIteratorPtr GetLinearEdgesOnConvexHull(Topo2d.Profile profile);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsIsCircle@4")]
        public static extern bool IsCircle(Topo2d.Profile profile);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsComputeTextOutline@4")]
        public static extern Topo2d.ProfilePtr ComputeTextOutline(Topo2d.Text text);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsComputeTextsOutline@4")]
        public static extern Topo2d.ProfilesPtr ComputeTextsOutline(Topo2d.Texts texts);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsGetTextLength@4")]
        public static extern double GetTextLength(Topo2d.Text text);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsGetTextsLength@4")]
        public static extern double GetTextsLength(Topo2d.Texts texts);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsComputeBestFittingPositionOfTextInFace@56")]
        public static extern bool ComputeBestFittingPositionOfTextInFace(Topo2d.Face face, double rotation, string text_1st_choice, string text_2nd_choice, double text_full_size, double text_reduced_size, bool display_anyway, out double out_fitting_position_x, out double out_fitting_position_y, System.Text.StringBuilder out_text_chosen, out double out_chosen_size);

    }
}

#pragma warning restore 169
#pragma warning restore 649
#pragma warning disable 169 // Suppress warning message for unused variable
#pragma warning disable 649 // Field is never assigned to, and will always have its default value null

namespace Alma.NetWrappersNew
{
    public partial class Topo2dTools
    {
        public class ListEdgeIteratorPtr
        {
            internal ListEdgeIteratorPtr() { }
            ~ListEdgeIteratorPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2dTools.DeleteListEdgeIteratorInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
            public System.IntPtr __Ptr;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsDeleteListEdgeIterator@4")]
        private static extern void DeleteListEdgeIteratorInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsListEdgeIteratorHasNext@4")]
        private static extern bool ListEdgeIteratorHasNextInternal(System.IntPtr x0);

        public static bool ListEdgeIteratorHasNext(ListEdgeIteratorPtr x0)
        {
            bool returned_value = ListEdgeIteratorHasNextInternal(x0.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsListEdgeIteratorNext@4")]
        private static extern System.IntPtr ListEdgeIteratorNextInternal(System.IntPtr x0);

        public static Topo2d.Edge ListEdgeIteratorNext(ListEdgeIteratorPtr x0)
        {
            System.IntPtr temp_object_value = ListEdgeIteratorNextInternal(x0.__Ptr);
            Topo2d.Edge temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Topo2d.Edge();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsGetMinimalHeightAngle@4")]
        private static extern double GetMinimalHeightAngleInternal(System.IntPtr profiles);

        public static double GetMinimalHeightAngle(Topo2d.Profiles profiles)
        {
            double returned_value = GetMinimalHeightAngleInternal(profiles.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsGetPointAtDistanceFromVertex@20")]
        private static extern bool GetPointAtDistanceFromVertexInternal(System.IntPtr vertex, double curvilinear_length, out double point_found_x, out double point_found_y);

        public static bool GetPointAtDistanceFromVertex(Topo2d.Vertex vertex, double curvilinear_length, out double point_found_x, out double point_found_y)
        {
            bool returned_value = GetPointAtDistanceFromVertexInternal(vertex.__Ptr, curvilinear_length, out point_found_x, out point_found_y);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsGetLinearEdgesOnConvexHull@4")]
        private static extern System.IntPtr GetLinearEdgesOnConvexHullInternal(System.IntPtr profile);

        public static ListEdgeIteratorPtr GetLinearEdgesOnConvexHull(Topo2d.Profile profile)
        {
            System.IntPtr temp_object_value = GetLinearEdgesOnConvexHullInternal(profile.__Ptr);
            ListEdgeIteratorPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new ListEdgeIteratorPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsIsCircle@4")]
        private static extern bool IsCircleInternal(System.IntPtr profile);

        public static bool IsCircle(Topo2d.Profile profile)
        {
            bool returned_value = IsCircleInternal(profile.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsComputeTextOutline@4")]
        private static extern System.IntPtr ComputeTextOutlineInternal(System.IntPtr text);

        public static Topo2d.ProfilePtr ComputeTextOutline(Topo2d.Text text)
        {
            System.IntPtr temp_object_value = ComputeTextOutlineInternal(text.__Ptr);
            Topo2d.ProfilePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Topo2d.ProfilePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsComputeTextsOutline@4")]
        private static extern System.IntPtr ComputeTextsOutlineInternal(System.IntPtr texts);

        public static Topo2d.ProfilesPtr ComputeTextsOutline(Topo2d.Texts texts)
        {
            System.IntPtr temp_object_value = ComputeTextsOutlineInternal(texts.__Ptr);
            Topo2d.ProfilesPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Topo2d.ProfilesPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsGetTextLength@4")]
        private static extern double GetTextLengthInternal(System.IntPtr text);

        public static double GetTextLength(Topo2d.Text text)
        {
            double returned_value = GetTextLengthInternal(text.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsGetTextsLength@4")]
        private static extern double GetTextsLengthInternal(System.IntPtr texts);

        public static double GetTextsLength(Topo2d.Texts texts)
        {
            double returned_value = GetTextsLengthInternal(texts.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dToolsComputeBestFittingPositionOfTextInFace@56")]
        private static extern bool ComputeBestFittingPositionOfTextInFaceInternal(System.IntPtr face, double rotation, string text_1st_choice, string text_2nd_choice, double text_full_size, double text_reduced_size, bool display_anyway, out double out_fitting_position_x, out double out_fitting_position_y, System.Text.StringBuilder out_text_chosen, out double out_chosen_size);

        public static bool ComputeBestFittingPositionOfTextInFace(Topo2d.Face face, double rotation, string text_1st_choice, string text_2nd_choice, double text_full_size, double text_reduced_size, bool display_anyway, out double out_fitting_position_x, out double out_fitting_position_y, System.Text.StringBuilder out_text_chosen, out double out_chosen_size)
        {
            bool returned_value = ComputeBestFittingPositionOfTextInFaceInternal(face.__Ptr, rotation, text_1st_choice, text_2nd_choice, text_full_size, text_reduced_size, display_anyway, out out_fitting_position_x, out out_fitting_position_y, out_text_chosen, out out_chosen_size);
            return returned_value;
        }

    }
}

#pragma warning restore 169
#pragma warning restore 649
