using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Alma.NetKernel.TranslationManager;
using Alma.NetKernel.UnitConverters;

namespace Alma.NetWrappers2D
{
    public static class Topo2DUtils
    {
        public static Topo2d.ProfilePtr CreateProfilePtr(long ptr)
        {
            if (ptr != 0)
            {
                Topo2d.ProfilePtr profilePtr;
                profilePtr = new Topo2d.ProfilePtr();
                profilePtr.__Ptr = new IntPtr(ptr);
                return profilePtr;
            }
            else
            {
                return null;
            }
        }
    }
}
