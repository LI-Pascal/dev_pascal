using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Mire
{
    public static class DefineAph
    {
        [DllImport("Aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "APH_getKeyPath")]
        public static extern int GetKeyPath(string key, StringBuilder filename, int maxLength);
        [DllImport("Aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "APH_getTempPath")]
        public static extern int GetTempPath(StringBuilder filename, int maxLength);
        [DllImport("Aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "APH_getRscPath")]
        public static extern int GetRscPath(StringBuilder filename, int maxLength);
        [DllImport("Aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "APH_getAppPath")]
        public static extern int GetAppPath(StringBuilder appPath, int maxLength);
        [DllImport("Aph.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "APH_getBinPath")]
        public static extern int GetBinPath(StringBuilder filename, int maxLength);
        const int utilStrMaxLen = 512;
        public static string getkeypath_(string key)
        {
            StringBuilder s = new StringBuilder("", utilStrMaxLen);
            if (GetKeyPath(key, s, utilStrMaxLen) != 0)
                return ResizeNullTerminatedStringNoTrim(s);
            else
                return string.Empty;
        }
        public static string getrscpath_()
        {
            StringBuilder s = new StringBuilder("", utilStrMaxLen);
            if (GetRscPath(s, utilStrMaxLen) != 0)
                return ResizeNullTerminatedStringNoTrim(s);
            else
                return string.Empty;
        }

        public static string ResizeNullTerminatedStringNoTrim(StringBuilder str)
        {
            var result = (str.ToString()).TrimEnd('\0').TrimEnd();
            return result;
        }
    }
}
