using System.Runtime.InteropServices;
#pragma warning disable 169 // Suppress warning message for unused variable
#pragma warning disable 649 // Field is never assigned to, and will always have its default value null

namespace Alma.NetWrappers
{
    public partial class Topo2d
    {
        public enum EdgeType {
                EdgeTypeSegment = 0,
                EdgeTypeArc = 1
        };

        public enum AttachmentType {
                TopLeft = 0,
                TopCenter = 1,
                TopRight = 2,
                MiddleLeft = 4,
                MiddleCenter = 5,
                MiddleRight = 6,
                BottomLeft = 8,
                BottomCenter = 9,
                BottomRight = 10
        };

        // Topo2dPoint is not a simple type.
        // Topo2dVector is not a simple type.
        public struct Vertex { internal System.IntPtr __Ptr; }
        public static bool IsNull(Vertex p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref Vertex p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref Vertex p, System.IntPtr value) { p.__Ptr = value; }
        public struct Edge { internal System.IntPtr __Ptr; }
        public static bool IsNull(Edge p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref Edge p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref Edge p, System.IntPtr value) { p.__Ptr = value; }
        public struct Profile { internal System.IntPtr __Ptr; }
        public static bool IsNull(Profile p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref Profile p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref Profile p, System.IntPtr value) { p.__Ptr = value; }
        public struct Profiles { internal System.IntPtr __Ptr; }
        public static bool IsNull(Profiles p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref Profiles p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref Profiles p, System.IntPtr value) { p.__Ptr = value; }
        public struct Face { internal System.IntPtr __Ptr; }
        public static bool IsNull(Face p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref Face p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref Face p, System.IntPtr value) { p.__Ptr = value; }
        public struct Faces { internal System.IntPtr __Ptr; }
        public static bool IsNull(Faces p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref Faces p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref Faces p, System.IntPtr value) { p.__Ptr = value; }
        public struct Part { internal System.IntPtr __Ptr; }
        public static bool IsNull(Part p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref Part p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref Part p, System.IntPtr value) { p.__Ptr = value; }
        public struct Parts { internal System.IntPtr __Ptr; }
        public static bool IsNull(Parts p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref Parts p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref Parts p, System.IntPtr value) { p.__Ptr = value; }
        public struct Text { internal System.IntPtr __Ptr; }
        public static bool IsNull(Text p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref Text p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref Text p, System.IntPtr value) { p.__Ptr = value; }
        public struct Texts { internal System.IntPtr __Ptr; }
        public static bool IsNull(Texts p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref Texts p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref Texts p, System.IntPtr value) { p.__Ptr = value; }
        public struct ProfilePtr { internal System.IntPtr __Ptr; }
        public static bool IsNull(ProfilePtr p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref ProfilePtr p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref ProfilePtr p, System.IntPtr value) { p.__Ptr = value; }
        public struct FacePtr { internal System.IntPtr __Ptr; }
        public static bool IsNull(FacePtr p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref FacePtr p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref FacePtr p, System.IntPtr value) { p.__Ptr = value; }
        public struct PartPtr { internal System.IntPtr __Ptr; }
        public static bool IsNull(PartPtr p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref PartPtr p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref PartPtr p, System.IntPtr value) { p.__Ptr = value; }
        public struct DprAttributePtr { internal System.IntPtr __Ptr; }
        public static bool IsNull(DprAttributePtr p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref DprAttributePtr p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref DprAttributePtr p, System.IntPtr value) { p.__Ptr = value; }
        public struct ProfilesPtr { internal System.IntPtr __Ptr; }
        public static bool IsNull(ProfilesPtr p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref ProfilesPtr p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref ProfilesPtr p, System.IntPtr value) { p.__Ptr = value; }
        public struct FacesPtr { internal System.IntPtr __Ptr; }
        public static bool IsNull(FacesPtr p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref FacesPtr p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref FacesPtr p, System.IntPtr value) { p.__Ptr = value; }
        public struct PartsPtr { internal System.IntPtr __Ptr; }
        public static bool IsNull(PartsPtr p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref PartsPtr p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref PartsPtr p, System.IntPtr value) { p.__Ptr = value; }
        public struct TextPtr { internal System.IntPtr __Ptr; }
        public static bool IsNull(TextPtr p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref TextPtr p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref TextPtr p, System.IntPtr value) { p.__Ptr = value; }
        public struct TextsPtr { internal System.IntPtr __Ptr; }
        public static bool IsNull(TextsPtr p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref TextsPtr p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref TextsPtr p, System.IntPtr value) { p.__Ptr = value; }
        public struct EdgeIteratorPtr { internal System.IntPtr __Ptr; }
        public static bool IsNull(EdgeIteratorPtr p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref EdgeIteratorPtr p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref EdgeIteratorPtr p, System.IntPtr value) { p.__Ptr = value; }
        public struct ProfileIteratorPtr { internal System.IntPtr __Ptr; }
        public static bool IsNull(ProfileIteratorPtr p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref ProfileIteratorPtr p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref ProfileIteratorPtr p, System.IntPtr value) { p.__Ptr = value; }
        public struct FaceIteratorPtr { internal System.IntPtr __Ptr; }
        public static bool IsNull(FaceIteratorPtr p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref FaceIteratorPtr p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref FaceIteratorPtr p, System.IntPtr value) { p.__Ptr = value; }
        public struct PartIteratorPtr { internal System.IntPtr __Ptr; }
        public static bool IsNull(PartIteratorPtr p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref PartIteratorPtr p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref PartIteratorPtr p, System.IntPtr value) { p.__Ptr = value; }
        public struct TextIteratorPtr { internal System.IntPtr __Ptr; }
        public static bool IsNull(TextIteratorPtr p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref TextIteratorPtr p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref TextIteratorPtr p, System.IntPtr value) { p.__Ptr = value; }
        public struct DprWriterPtr { internal System.IntPtr __Ptr; }
        public static bool IsNull(DprWriterPtr p) { return p.__Ptr == System.IntPtr.Zero; }
        public static void SetNull(ref DprWriterPtr p) { p.__Ptr = System.IntPtr.Zero; }
        public static void SetValue(ref DprWriterPtr p, System.IntPtr value) { p.__Ptr = value; }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewPart@0")]
        public static extern PartPtr NewPart();

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeepCopyPart@4")]
        public static extern PartPtr DeepCopyPart(Part part);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeepCopyPartNoAttribute@4")]
        public static extern PartPtr DeepCopyPartNoAttribute(Part part);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dPartNewSharedOwner@4")]
        public static extern PartPtr PartNewSharedOwner(PartPtr part);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dAddFace@8")]
        public static extern void AddFace(PartPtr part, FacePtr face);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeletePart@4")]
        public static extern void DeletePart(PartPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewFace@4")]
        public static extern FacePtr NewFace(ProfilePtr externalProfile);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeepCopyFace@4")]
        public static extern FacePtr DeepCopyFace(Face face);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFaceNewSharedOwner@4")]
        public static extern FacePtr FaceNewSharedOwner(FacePtr face);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dAddInternalProfile@8")]
        public static extern void AddInternalProfile(FacePtr face, ProfilePtr profile);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFaceRemoveTexts@4")]
        public static extern void FaceRemoveTexts(FacePtr face);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteFace@4")]
        public static extern void DeleteFace(FacePtr x0);

        // Topo2dNewProfile is not a simple function.

        // Topo2dNewProfileEpsilon is not a simple function.

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeepCopyProfile@4")]
        public static extern ProfilePtr DeepCopyProfile(Profile profile);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileNewSharedOwner@4")]
        public static extern ProfilePtr ProfileNewSharedOwner(ProfilePtr profile);

        // Topo2dAddSegment is not a simple function.

        // Topo2dAddArc is not a simple function.

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dCloseProfile@4")]
        public static extern bool CloseProfile(ProfilePtr profil);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteProfile@4")]
        public static extern void DeleteProfile(ProfilePtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileGetEpsilon@4")]
        public static extern double ProfileGetEpsilon(ProfilePtr profile);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileSetEpsilon@12")]
        public static extern void ProfileSetEpsilon(ProfilePtr profile, double epsilon);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeepCopyText@4")]
        public static extern TextPtr DeepCopyText(Text text);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeepCopyTexts@4")]
        public static extern TextsPtr DeepCopyTexts(Texts texts);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteDprAttributePtr@4")]
        public static extern void DeleteDprAttributePtr(DprAttributePtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetFaces@4")]
        public static extern Faces GetFaces(PartPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetFacesOnWeak@4")]
        public static extern Faces GetFacesOnWeak(Part x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetExternalProfile@4")]
        public static extern Profile GetExternalProfile(FacePtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetExternalProfileOnWeak@4")]
        public static extern Profile GetExternalProfileOnWeak(Face x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetInternalProfiles@4")]
        public static extern Profiles GetInternalProfiles(FacePtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetInternalProfilesOnWeak@4")]
        public static extern Profiles GetInternalProfilesOnWeak(Face x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetTexts@4")]
        public static extern Texts GetTexts(FacePtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetTextsOnWeak@4")]
        public static extern Texts GetTextsOnWeak(Face x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewEdgeIterator@4")]
        public static extern EdgeIteratorPtr NewEdgeIterator(ProfilePtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewEdgeIteratorOnWeak@4")]
        public static extern EdgeIteratorPtr NewEdgeIteratorOnWeak(Profile x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteEdgeIterator@4")]
        public static extern void DeleteEdgeIterator(EdgeIteratorPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dEdgeIteratorHasNext@4")]
        public static extern bool EdgeIteratorHasNext(EdgeIteratorPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dEdgeIteratorNext@4")]
        public static extern Edge EdgeIteratorNext(EdgeIteratorPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetEdgeType@4")]
        public static extern EdgeType GetEdgeType(Edge x0);

        // Topo2dGetSegment is not a simple function.

        // Topo2dGetArc is not a simple function.

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetPreviousVertex@4")]
        public static extern Vertex GetPreviousVertex(Edge edge);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetNextVertex@4")]
        public static extern Vertex GetNextVertex(Edge edge);

        // Topo2dGetPosition is not a simple function.

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertProfile@4")]
        public static extern Profile ConvertProfile(ProfilePtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertProfiles@4")]
        public static extern Profiles ConvertProfiles(ProfilesPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertFace@4")]
        public static extern Face ConvertFace(FacePtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertFaces@4")]
        public static extern Faces ConvertFaces(FacesPtr faces);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertPart@4")]
        public static extern Part ConvertPart(PartPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertParts@4")]
        public static extern Parts ConvertParts(PartsPtr parts);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertText@4")]
        public static extern Text ConvertText(TextPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertTexts@4")]
        public static extern Texts ConvertTexts(TextsPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewParts@0")]
        public static extern PartsPtr NewParts();

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteParts@4")]
        public static extern void DeleteParts(PartsPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dInsertPartPtr@8")]
        public static extern void InsertPartPtr(PartsPtr x0, PartPtr x1);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewFaces@0")]
        public static extern FacesPtr NewFaces();

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteFaces@4")]
        public static extern void DeleteFaces(FacesPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dInsertFacePtr@8")]
        public static extern void InsertFacePtr(FacesPtr x0, FacePtr x1);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewProfiles@0")]
        public static extern ProfilesPtr NewProfiles();

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteProfiles@4")]
        public static extern void DeleteProfiles(ProfilesPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dInsertProfilePtr@8")]
        public static extern void InsertProfilePtr(ProfilesPtr x0, ProfilePtr x1);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewTexts@0")]
        public static extern TextsPtr NewTexts();

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteTexts@4")]
        public static extern void DeleteTexts(TextsPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dInsertTextPtr@8")]
        public static extern void InsertTextPtr(TextsPtr texts, TextPtr text);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewPartIterator@4")]
        public static extern PartIteratorPtr NewPartIterator(Parts x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeletePartIterator@4")]
        public static extern void DeletePartIterator(PartIteratorPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dPartIteratorHasNext@4")]
        public static extern bool PartIteratorHasNext(PartIteratorPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dPartIteratorNext@4")]
        public static extern Part PartIteratorNext(PartIteratorPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewFaceIterator@4")]
        public static extern FaceIteratorPtr NewFaceIterator(Faces x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteFaceIterator@4")]
        public static extern void DeleteFaceIterator(FaceIteratorPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFaceIteratorHasNext@4")]
        public static extern bool FaceIteratorHasNext(FaceIteratorPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFaceIteratorNext@4")]
        public static extern Face FaceIteratorNext(FaceIteratorPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewProfileIterator@4")]
        public static extern ProfileIteratorPtr NewProfileIterator(Profiles x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteProfileIterator@4")]
        public static extern void DeleteProfileIterator(ProfileIteratorPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileIteratorHasNext@4")]
        public static extern bool ProfileIteratorHasNext(ProfileIteratorPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileIteratorNext@4")]
        public static extern Profile ProfileIteratorNext(ProfileIteratorPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewTextIterator@4")]
        public static extern TextIteratorPtr NewTextIterator(Texts x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteTextIterator@4")]
        public static extern void DeleteTextIterator(TextIteratorPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTextIteratorHasNext@4")]
        public static extern bool TextIteratorHasNext(TextIteratorPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTextIteratorNext@4")]
        public static extern Text TextIteratorNext(TextIteratorPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWriteProfileP01@8")]
        public static extern void WriteProfileP01(Profile profile, string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWriteFaceP01@8")]
        public static extern void WriteFaceP01(Face face, string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWritePartP01@8")]
        public static extern void WritePartP01(Part part, string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadPartPtrP01@4")]
        public static extern PartPtr ReadPartPtrP01(string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadProfilesPtrP01@4")]
        public static extern ProfilesPtr ReadProfilesPtrP01(string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadProfilesPtrDpr@4")]
        public static extern ProfilesPtr ReadProfilesPtrDpr(string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadTextsPtrDpr@4")]
        public static extern TextsPtr ReadTextsPtrDpr(string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadFacePtrDpr@4")]
        public static extern FacePtr ReadFacePtrDpr(string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadPartPtrDpr@4")]
        public static extern PartPtr ReadPartPtrDpr(string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadCutPartPtrDpr@4")]
        public static extern PartPtr ReadCutPartPtrDpr(string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadDprAttribute@4")]
        public static extern DprAttributePtr ReadDprAttribute(string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWriteProfileDpr@8")]
        public static extern void WriteProfileDpr(Profile profile, string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWriteProfilesDpr@8")]
        public static extern void WriteProfilesDpr(Profiles profiles, string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWriteFaceDpr@8")]
        public static extern void WriteFaceDpr(Face face, string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWritePartDpr@8")]
        public static extern void WritePartDpr(Part part, string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterNew@4")]
        public static extern DprWriterPtr DprWriterNew(string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterDelete@4")]
        public static extern void DprWriterDelete(DprWriterPtr writer);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterSetDprAttribute@8")]
        public static extern void DprWriterSetDprAttribute(DprWriterPtr writer, DprAttributePtr dpr_attribute);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterSetOrientation@8")]
        public static extern void DprWriterSetOrientation(DprWriterPtr writer, bool material_left);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterWriteProfile@8")]
        public static extern int DprWriterWriteProfile(DprWriterPtr writer, Profile profile);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterWriteProfiles@8")]
        public static extern int DprWriterWriteProfiles(DprWriterPtr writer, Profiles profiles);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterWriteFace@8")]
        public static extern int DprWriterWriteFace(DprWriterPtr writer, Face face);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterWritePart@8")]
        public static extern int DprWriterWritePart(DprWriterPtr writer, Part part);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWriteProfile@8")]
        public static extern void WriteProfile(Profile profile, string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadProfile@4")]
        public static extern ProfilePtr ReadProfile(string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWritePart@8")]
        public static extern void WritePart(Part part, string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadPart@4")]
        public static extern PartPtr ReadPart(string filename);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWritePartString@16")]
        public static extern int WritePartString(Part part, string buffer, int buffer_size, out int actual_size);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadPartString@8")]
        public static extern PartPtr ReadPartString(string buffer, int buffer_size);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewProfileXY@16")]
        public static extern ProfilePtr NewProfileXY(double x, double y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dAddSegmentXY@20")]
        public static extern Edge AddSegmentXY(ProfilePtr profile, double x, double y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dAddArcXY@40")]
        public static extern Edge AddArcXY(ProfilePtr profile, double x, double y, double x_center, double y_center, bool trigo);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dAddArcXYArrow@28")]
        public static extern Edge AddArcXYArrow(ProfilePtr profile, double x, double y, double arrow);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetSegmentXY@20")]
        public static extern void GetSegmentXY(Edge edge, out double start_x, out double start_y, out double end_x, out double end_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetArcXY@32")]
        public static extern void GetArcXY(Edge edge, out double start_x, out double start_y, out double end_x, out double end_y, out double center_x, out double center_y, out bool trigo);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetPartBoundingBox@20")]
        public static extern void GetPartBoundingBox(Part part, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetFaceBoundingBox@20")]
        public static extern void GetFaceBoundingBox(Face face, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetProfileBoundingBox@20")]
        public static extern void GetProfileBoundingBox(Profile profile, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetProfilesBoundingBox@20")]
        public static extern void GetProfilesBoundingBox(Profiles profile, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetTextBoundingBox@20")]
        public static extern void GetTextBoundingBox(Text text, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetFaceMinSize@12")]
        public static extern void GetFaceMinSize(Face face, out double length, out double width);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetFaceArea@4")]
        public static extern double GetFaceArea(Face face);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetPartArea@4")]
        public static extern double GetPartArea(Part part);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetProfileLength@4")]
        public static extern double GetProfileLength(Profile profile);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetProfileIsClosed@4")]
        public static extern bool GetProfileIsClosed(Profile profile);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetProfileArea@4")]
        public static extern double GetProfileArea(Profile profile);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetEdgeLength@4")]
        public static extern double GetEdgeLength(Edge edge);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dIdentifyPartWithRectangularExternal@4")]
        public static extern bool IdentifyPartWithRectangularExternal(Part part);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dRotatePart@28")]
        public static extern void RotatePart(PartPtr part, double angle, double pivot_x, double pivot_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dRotateFace@28")]
        public static extern void RotateFace(FacePtr face, double angle, double pivot_x, double pivot_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dRotateProfile@28")]
        public static extern void RotateProfile(ProfilePtr profile, double angle, double pivot_x, double pivot_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dRotateText@28")]
        public static extern void RotateText(TextPtr text, double angle, double pivot_x, double pivot_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTranslatePart@20")]
        public static extern void TranslatePart(PartPtr part, double x_move, double y_move);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTranslateFace@20")]
        public static extern void TranslateFace(FacePtr face, double x_move, double y_move);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTranslateProfile@20")]
        public static extern void TranslateProfile(ProfilePtr profile, double x_move, double y_move);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTranslateText@20")]
        public static extern void TranslateText(TextPtr text, double x_move, double y_move);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSymmetrizePart@36")]
        public static extern void SymmetrizePart(PartPtr part, double x_base, double y_base, double x_vector, double y_vector);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSymmetrizeFace@36")]
        public static extern void SymmetrizeFace(FacePtr face, double x_base, double y_base, double x_vector, double y_vector);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSymmetrizeProfile@36")]
        public static extern void SymmetrizeProfile(ProfilePtr profile, double x_base, double y_base, double x_vector, double y_vector);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSymmetrizeText@36")]
        public static extern void SymmetrizeText(TextPtr text, double x_base, double y_base, double x_vector, double y_vector);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTranslateBBBottomLeftToOrigin@4")]
        public static extern void TranslateBBBottomLeftToOrigin(ProfilePtr profile);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileCornerBottomLeft@4")]
        public static extern int ProfileCornerBottomLeft(ProfilePtr profile);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileRightAngleAtCorner@8")]
        public static extern int ProfileRightAngleAtCorner(ProfilePtr profile, int wanted_corner);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFilletXY@28")]
        public static extern bool FilletXY(ProfilePtr profile, double x, double y, double radius);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFilletAll@28")]
        public static extern void FilletAll(ProfilePtr profile, double radius, double minThreshold, double maxThreshold);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dIsInsideProfile@32")]
        public static extern int IsInsideProfile(ProfilePtr profile, double point_x, double point_y, int boundary_accepted, double max_distance);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dIsInsideFace@36")]
        public static extern int IsInsideFace(FacePtr face, double point_x, double point_y, int external_boundary_accepted, int internal_boundary_accepted, double max_distance);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfilesIntersect@8")]
        public static extern bool ProfilesIntersect(ProfilePtr profile1, ProfilePtr profile2);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileSplitOnIntersections@4")]
        public static extern ProfilesPtr ProfileSplitOnIntersections(ProfilePtr profile);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGridCutting@40")]
        public static extern ProfilesPtr GridCutting(Part part, double dx, double dy, double leadin_distance, double border_distance, bool smallest_side_first);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGridCuttingMinLengthLines@40")]
        public static extern ProfilesPtr GridCuttingMinLengthLines(Part part, double dx, double dy, double leadin_distance, double border_distance, bool smallest_side_first);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileFindArcs@12")]
        public static extern void ProfileFindArcs(ProfilePtr profile, double epsilon);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeCircleFaceXY@8")]
        public static extern FacePtr MakeCircleFaceXY(double radius);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakePolygonEllipseFace@20")]
        public static extern FacePtr MakePolygonEllipseFace(double big_radius, double small_radius, int nb_sides);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeArcsEllipseProfile@20")]
        public static extern ProfilePtr MakeArcsEllipseProfile(double big_radius, double small_radius, int arcs_number);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeCircleFaceWithPositionedCircleHoleXY@24")]
        public static extern FacePtr MakeCircleFaceWithPositionedCircleHoleXY(double radius_ext, double radius_int, double offset);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeCircleFaceWithCenteredRingOfHoleXY@36")]
        public static extern FacePtr MakeCircleFaceWithCenteredRingOfHoleXY(double radius_ext, double radius_ring, double radius_hole, uint N, double radius_int);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeCircleFaceWith2CenteredRingsOfHoleXY@80")]
        public static extern FacePtr MakeCircleFaceWith2CenteredRingsOfHoleXY(double radius_ext, double radius_int, double radius_ring1, double radius_hole1, double theta, uint N1, double radius_ring2, double radius_hole2, double angle_start2, double angle_move2, uint N2);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeBrokenRingFaceWith2CenteredRingsOfHoleXY@96")]
        public static extern FacePtr MakeBrokenRingFaceWith2CenteredRingsOfHoleXY(double radius_ext, double radius_int, double theta, double radius_ring1, double radius_hole1, double angle_start1, double angle_move1, uint N1, double radius_ring2, double radius_hole2, double angle_start2, double angle_move2, uint N2);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeBrokenRingFaceXY@24")]
        public static extern FacePtr MakeBrokenRingFaceXY(double radius_ext, double radius_int, double theta);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeTriangleFaceXY@48")]
        public static extern FacePtr MakeTriangleFaceXY(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleProfileXY@32")]
        public static extern ProfilePtr MakeRectangleProfileXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceXY@32")]
        public static extern FacePtr MakeRectangleFaceXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithHoleXY@40")]
        public static extern FacePtr MakeRectangleFaceWithHoleXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double radius);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithPositionedRectangleHoleXY@64")]
        public static extern FacePtr MakeRectangleFaceWithPositionedRectangleHoleXY(double ext_bottom_left_x, double ext_bottom_left_y, double ext_top_right_x, double ext_top_right_y, double int_bottom_left_x, double int_bottom_left_y, double int_top_right_x, double int_top_right_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithGridHoleXY@56")]
        public static extern FacePtr MakeRectangleFaceWithGridHoleXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, double dy, double radius);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithGridNMHoleXY@64")]
        public static extern FacePtr MakeRectangleFaceWithGridNMHoleXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, double dy, int nX, int nY, double radius);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithGridNMHoleOffsetXY@80")]
        public static extern FacePtr MakeRectangleFaceWithGridNMHoleOffsetXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double offset_x, double offset_y, double dX, double dY, int nX, int nY, double radius);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithOblongsAlignXY@56")]
        public static extern FacePtr MakeRectangleFaceWithOblongsAlignXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, double length, double radius);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithNOblongsAlignXY@60")]
        public static extern FacePtr MakeRectangleFaceWithNOblongsAlignXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, int nX, double length, double radius);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithNOblongsAlignOffsetXY@76")]
        public static extern FacePtr MakeRectangleFaceWithNOblongsAlignOffsetXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double offset_x, double offset_y, double dX, int nX, double length, double radius);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRoundRectangleFaceXY@40")]
        public static extern FacePtr MakeRoundRectangleFaceXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRoundRectangleFaceWithHoleXY@48")]
        public static extern FacePtr MakeRoundRectangleFaceWithHoleXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double radius);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRoundRectangleFaceWithOblongXY@56")]
        public static extern FacePtr MakeRoundRectangleFaceWithOblongXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double oblong_length, double oblong_radius);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRoundRectangleFaceWithFiveHolesXY@72")]
        public static extern FacePtr MakeRoundRectangleFaceWithFiveHolesXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double radiusCorners, double radiusCenterHole, double dX, double dY, double radiusHolesInCorners);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeEatenRectangleFaceXY@40")]
        public static extern FacePtr MakeEatenRectangleFaceXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeEatenRectangleFaceWithHoleXY@48")]
        public static extern FacePtr MakeEatenRectangleFaceWithHoleXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double radius);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeEatenRectangleFaceWithOblongXY@56")]
        public static extern FacePtr MakeEatenRectangleFaceWithOblongXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double chamfer_value, double oblong_length, double oblong_radius);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeChamferRectangleFaceXY@40")]
        public static extern FacePtr MakeChamferRectangleFaceXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double chamfer_value);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeEatenTrapezoidXY@80")]
        public static extern FacePtr MakeEatenTrapezoidXY(double bottom_left_x, double bottom_left_y, double bottom_right_x, double bottom_right_y, double top_left_x, double top_left_y, double top_right_x, double top_right_y, double top_offset, double radius);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeChamferRectangleFaceWithHoleXY@48")]
        public static extern FacePtr MakeChamferRectangleFaceWithHoleXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double chamfer_value, double radius);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeChamferRectangleFaceWithOblongXY@56")]
        public static extern FacePtr MakeChamferRectangleFaceWithOblongXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double oblong_length, double chamfer_value);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeOblongFaceXY@32")]
        public static extern FacePtr MakeOblongFaceXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeLFaceXY@48")]
        public static extern FacePtr MakeLFaceXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double thicknessH, double thicknessV);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeTFaceXY@32")]
        public static extern FacePtr MakeTFaceXY(double length, double width, double thicknessH, double thicknessV);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeCamXY@56")]
        public static extern FacePtr MakeCamXY(double spacing_ext, double radius_ext_left, double radius_ext_right, double pos_int_left, double radius_int_left, double pos_int_right, double radius_int_right);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeKeyXY@48")]
        public static extern FacePtr MakeKeyXY(double spacing, double radius_ext, double radius_int, double radius, double thickness, double radius_round);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRodXY@56")]
        public static extern FacePtr MakeRodXY(double spacing, double radius_ext_left, double radius_ext_right, double radius_int_left, double radius_int_right, double thickness, double radius_round);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakePolygone5FaceXY@80")]
        public static extern FacePtr MakePolygone5FaceXY(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y, double p4_x, double p4_y, double p5_x, double p5_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakePolygone6FaceXY@96")]
        public static extern FacePtr MakePolygone6FaceXY(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y, double p4_x, double p4_y, double p5_x, double p5_y, double p6_x, double p6_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakePolygone8FaceXY@128")]
        public static extern FacePtr MakePolygone8FaceXY(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y, double p4_x, double p4_y, double p5_x, double p5_y, double p6_x, double p6_y, double p7_x, double p7_y, double p8_x, double p8_y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFaceHasIntersections@4")]
        public static extern bool FaceHasIntersections(FacePtr face);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewSimpleText@8")]
        public static extern TextPtr NewSimpleText(string text, string font);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewTextXY@40")]
        public static extern TextPtr NewTextXY(string text, double x, double y, double angle, string font, double font_height);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteText@4")]
        public static extern void DeleteText(TextPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dAddTextToFace@8")]
        public static extern void AddTextToFace(Face face, TextPtr text);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetTextString@12")]
        public static extern void GetTextString(Text text, System.Text.StringBuilder out_string, int string_size);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTextGetAngle@4")]
        public static extern double TextGetAngle(Text text);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTextGetHeight@4")]
        public static extern double TextGetHeight(Text text);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTextGetPoint@12")]
        public static extern void TextGetPoint(Text text, out double x, out double y);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetTextFont@12")]
        public static extern void GetTextFont(Text text, System.Text.StringBuilder out_font_name, int font_name_size);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSetTextHeight@12")]
        public static extern void SetTextHeight(Text text, double height);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSetTextAttachmentType@8")]
        public static extern void SetTextAttachmentType(Text text, AttachmentType attachment_type);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dRenderText@4")]
        public static extern PartPtr RenderText(Text text);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSetMachiningOnProfile@8")]
        public static extern void SetMachiningOnProfile(Profile profile, int machining);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetMachiningOfProfile@8")]
        public static extern int GetMachiningOfProfile(Profile profile, out int out_machining);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dIsProfileIgnoredByNesting@8")]
        public static extern int IsProfileIgnoredByNesting(Profile profile, out bool out_nesting_ignored);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSetMachiningOnText@8")]
        public static extern void SetMachiningOnText(Text text, int machining);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetMachiningOfText@8")]
        public static extern int GetMachiningOfText(Text text, out int out_machining);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSetBevelAttributeOnEdge@8")]
        public static extern int SetBevelAttributeOnEdge(Edge edge, int bevel_type);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dRemoveBevelAttributeOnEdge@4")]
        public static extern void RemoveBevelAttributeOnEdge(Edge edge);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSetFieldOfBevelAttributeOnEdge@16")]
        public static extern int SetFieldOfBevelAttributeOnEdge(Edge edge, int field, double value);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetBevelTypeOnEdge@4")]
        public static extern int GetBevelTypeOnEdge(Edge edge);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetFieldValueOfBevelAttributeOnEdge@12")]
        public static extern int GetFieldValueOfBevelAttributeOnEdge(Edge edge, int field, out double out_value);

    }
}

#pragma warning restore 169
#pragma warning restore 649
#pragma warning disable 169 // Suppress warning message for unused variable
#pragma warning disable 649 // Field is never assigned to, and will always have its default value null

namespace Alma.NetWrappersNew
{
    public partial class Topo2d
    {
        public enum EdgeType {
                EdgeTypeSegment = 0,
                EdgeTypeArc = 1
        };

        public enum AttachmentType {
                TopLeft = 0,
                TopCenter = 1,
                TopRight = 2,
                MiddleLeft = 4,
                MiddleCenter = 5,
                MiddleRight = 6,
                BottomLeft = 8,
                BottomCenter = 9,
                BottomRight = 10
        };

        // Topo2dPoint is not a simple type.
        // Topo2dVector is not a simple type.
        public class Vertex
        {
            internal Vertex() { }
            ~Vertex()
            {}
            public Vertex(System.IntPtr ptr) { __Ptr = ptr; }
            public System.IntPtr __Ptr;
        }
        public class Edge
        {
            internal Edge() { }
            ~Edge()
            {}
            public Edge(System.IntPtr ptr) { __Ptr = ptr; }
            public System.IntPtr __Ptr;
        }
        public class Profile
        {
            internal Profile() { }
            ~Profile()
            {}
            public Profile(System.IntPtr ptr) { __Ptr = ptr; }
            public System.IntPtr __Ptr;
        }
        public class Profiles
        {
            internal Profiles() { }
            ~Profiles()
            {}
            public Profiles(System.IntPtr ptr) { __Ptr = ptr; }
            public System.IntPtr __Ptr;
        }
        public class Face
        {
            internal Face() { }
            ~Face()
            {}
            public Face(System.IntPtr ptr) { __Ptr = ptr; }
            public System.IntPtr __Ptr;
        }
        public class Faces
        {
            internal Faces() { }
            ~Faces()
            {}
            public Faces(System.IntPtr ptr) { __Ptr = ptr; }
            public System.IntPtr __Ptr;
        }
        public class Part
        {
            internal Part() { }
            ~Part()
            {}
            public Part(System.IntPtr ptr) { __Ptr = ptr; }
            public System.IntPtr __Ptr;
        }
        public class Parts
        {
            internal Parts() { }
            ~Parts()
            {}
            public Parts(System.IntPtr ptr) { __Ptr = ptr; }
            public System.IntPtr __Ptr;
        }
        public class Text
        {
            internal Text() { }
            ~Text()
            {}
            public Text(System.IntPtr ptr) { __Ptr = ptr; }
            public System.IntPtr __Ptr;
        }
        public class Texts
        {
            internal Texts() { }
            ~Texts()
            {}
            public Texts(System.IntPtr ptr) { __Ptr = ptr; }
            public System.IntPtr __Ptr;
        }
        public class ProfilePtr
        {
            internal ProfilePtr() { }
            ~ProfilePtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteProfileInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
            public System.IntPtr __Ptr;
        }
        public class FacePtr
        {
            internal FacePtr() { }
            ~FacePtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteFaceInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
            public System.IntPtr __Ptr;
        }
        public class PartPtr
        {
            internal PartPtr() { }
            ~PartPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeletePartInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
            public System.IntPtr __Ptr;
        }
        public class DprAttributePtr
        {
            internal DprAttributePtr() { }
            ~DprAttributePtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteDprAttributePtrInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
            public System.IntPtr __Ptr;
        }
        public class ProfilesPtr
        {
            internal ProfilesPtr() { }
            ~ProfilesPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteProfilesInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
            public System.IntPtr __Ptr;
        }
        public class FacesPtr
        {
            internal FacesPtr() { }
            ~FacesPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteFacesInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
            public System.IntPtr __Ptr;
        }
        public class PartsPtr
        {
            internal PartsPtr() { }
            ~PartsPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeletePartsInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
            public System.IntPtr __Ptr;
        }
        public class TextPtr
        {
            internal TextPtr() { }
            ~TextPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteTextInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
            public System.IntPtr __Ptr;
        }
        public class TextsPtr
        {
            internal TextsPtr() { }
            ~TextsPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteTextsInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
            public System.IntPtr __Ptr;
        }
        public class EdgeIteratorPtr
        {
            internal EdgeIteratorPtr() { }
            ~EdgeIteratorPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteEdgeIteratorInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
            public System.IntPtr __Ptr;
        }
        public class ProfileIteratorPtr
        {
            internal ProfileIteratorPtr() { }
            ~ProfileIteratorPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteProfileIteratorInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
            public System.IntPtr __Ptr;
        }
        public class FaceIteratorPtr
        {
            internal FaceIteratorPtr() { }
            ~FaceIteratorPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteFaceIteratorInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
            public System.IntPtr __Ptr;
        }
        public class PartIteratorPtr
        {
            internal PartIteratorPtr() { }
            ~PartIteratorPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeletePartIteratorInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
            public System.IntPtr __Ptr;
        }
        public class TextIteratorPtr
        {
            internal TextIteratorPtr() { }
            ~TextIteratorPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteTextIteratorInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
            public System.IntPtr __Ptr;
        }
        public class DprWriterPtr
        {
            internal DprWriterPtr() { }
            ~DprWriterPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DprWriterDeleteInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
            public System.IntPtr __Ptr;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewPart@0")]
        private static extern System.IntPtr NewPartInternal();

        public static PartPtr NewPart()
        {
            System.IntPtr temp_object_value = NewPartInternal();
            PartPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new PartPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeepCopyPart@4")]
        private static extern System.IntPtr DeepCopyPartInternal(System.IntPtr part);

        public static PartPtr DeepCopyPart(Part part)
        {
            System.IntPtr temp_object_value = DeepCopyPartInternal(part.__Ptr);
            PartPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new PartPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeepCopyPartNoAttribute@4")]
        private static extern System.IntPtr DeepCopyPartNoAttributeInternal(System.IntPtr part);

        public static PartPtr DeepCopyPartNoAttribute(Part part)
        {
            System.IntPtr temp_object_value = DeepCopyPartNoAttributeInternal(part.__Ptr);
            PartPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new PartPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dPartNewSharedOwner@4")]
        private static extern System.IntPtr PartNewSharedOwnerInternal(System.IntPtr part);

        public static PartPtr PartNewSharedOwner(PartPtr part)
        {
            System.IntPtr temp_object_value = PartNewSharedOwnerInternal(part.__Ptr);
            PartPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new PartPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dAddFace@8")]
        private static extern void AddFaceInternal(System.IntPtr part, System.IntPtr face);

        public static void AddFace(PartPtr part, FacePtr face)
        {
            AddFaceInternal(part.__Ptr, face.__Ptr);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeletePart@4")]
        private static extern void DeletePartInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewFace@4")]
        private static extern System.IntPtr NewFaceInternal(System.IntPtr externalProfile);

        public static FacePtr NewFace(ProfilePtr externalProfile)
        {
            System.IntPtr temp_object_value = NewFaceInternal(externalProfile.__Ptr);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeepCopyFace@4")]
        private static extern System.IntPtr DeepCopyFaceInternal(System.IntPtr face);

        public static FacePtr DeepCopyFace(Face face)
        {
            System.IntPtr temp_object_value = DeepCopyFaceInternal(face.__Ptr);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFaceNewSharedOwner@4")]
        private static extern System.IntPtr FaceNewSharedOwnerInternal(System.IntPtr face);

        public static FacePtr FaceNewSharedOwner(FacePtr face)
        {
            System.IntPtr temp_object_value = FaceNewSharedOwnerInternal(face.__Ptr);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dAddInternalProfile@8")]
        private static extern void AddInternalProfileInternal(System.IntPtr face, System.IntPtr profile);

        public static void AddInternalProfile(FacePtr face, ProfilePtr profile)
        {
            AddInternalProfileInternal(face.__Ptr, profile.__Ptr);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFaceRemoveTexts@4")]
        private static extern void FaceRemoveTextsInternal(System.IntPtr face);

        public static void FaceRemoveTexts(FacePtr face)
        {
            FaceRemoveTextsInternal(face.__Ptr);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteFace@4")]
        private static extern void DeleteFaceInternal(System.IntPtr x0);

        // Topo2dNewProfile is not a simple function.

        // Topo2dNewProfileEpsilon is not a simple function.

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeepCopyProfile@4")]
        private static extern System.IntPtr DeepCopyProfileInternal(System.IntPtr profile);

        public static ProfilePtr DeepCopyProfile(Profile profile)
        {
            System.IntPtr temp_object_value = DeepCopyProfileInternal(profile.__Ptr);
            ProfilePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new ProfilePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileNewSharedOwner@4")]
        private static extern System.IntPtr ProfileNewSharedOwnerInternal(System.IntPtr profile);

        public static ProfilePtr ProfileNewSharedOwner(ProfilePtr profile)
        {
            System.IntPtr temp_object_value = ProfileNewSharedOwnerInternal(profile.__Ptr);
            ProfilePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new ProfilePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        // Topo2dAddSegment is not a simple function.

        // Topo2dAddArc is not a simple function.

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dCloseProfile@4")]
        private static extern bool CloseProfileInternal(System.IntPtr profil);

        public static bool CloseProfile(ProfilePtr profil)
        {
            bool returned_value = CloseProfileInternal(profil.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteProfile@4")]
        private static extern void DeleteProfileInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileGetEpsilon@4")]
        private static extern double ProfileGetEpsilonInternal(System.IntPtr profile);

        public static double ProfileGetEpsilon(ProfilePtr profile)
        {
            double returned_value = ProfileGetEpsilonInternal(profile.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileSetEpsilon@12")]
        private static extern void ProfileSetEpsilonInternal(System.IntPtr profile, double epsilon);

        public static void ProfileSetEpsilon(ProfilePtr profile, double epsilon)
        {
            ProfileSetEpsilonInternal(profile.__Ptr, epsilon);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeepCopyText@4")]
        private static extern System.IntPtr DeepCopyTextInternal(System.IntPtr text);

        public static TextPtr DeepCopyText(Text text)
        {
            System.IntPtr temp_object_value = DeepCopyTextInternal(text.__Ptr);
            TextPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new TextPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeepCopyTexts@4")]
        private static extern System.IntPtr DeepCopyTextsInternal(System.IntPtr texts);

        public static TextsPtr DeepCopyTexts(Texts texts)
        {
            System.IntPtr temp_object_value = DeepCopyTextsInternal(texts.__Ptr);
            TextsPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new TextsPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteDprAttributePtr@4")]
        private static extern void DeleteDprAttributePtrInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetFaces@4")]
        private static extern System.IntPtr GetFacesInternal(System.IntPtr x0);

        public static Faces GetFaces(PartPtr x0)
        {
            System.IntPtr temp_object_value = GetFacesInternal(x0.__Ptr);
            Faces temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Faces();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetFacesOnWeak@4")]
        private static extern System.IntPtr GetFacesOnWeakInternal(System.IntPtr x0);

        public static Faces GetFacesOnWeak(Part x0)
        {
            System.IntPtr temp_object_value = GetFacesOnWeakInternal(x0.__Ptr);
            Faces temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Faces();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetExternalProfile@4")]
        private static extern System.IntPtr GetExternalProfileInternal(System.IntPtr x0);

        public static Profile GetExternalProfile(FacePtr x0)
        {
            System.IntPtr temp_object_value = GetExternalProfileInternal(x0.__Ptr);
            Profile temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Profile();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetExternalProfileOnWeak@4")]
        private static extern System.IntPtr GetExternalProfileOnWeakInternal(System.IntPtr x0);

        public static Profile GetExternalProfileOnWeak(Face x0)
        {
            System.IntPtr temp_object_value = GetExternalProfileOnWeakInternal(x0.__Ptr);
            Profile temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Profile();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetInternalProfiles@4")]
        private static extern System.IntPtr GetInternalProfilesInternal(System.IntPtr x0);

        public static Profiles GetInternalProfiles(FacePtr x0)
        {
            System.IntPtr temp_object_value = GetInternalProfilesInternal(x0.__Ptr);
            Profiles temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Profiles();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetInternalProfilesOnWeak@4")]
        private static extern System.IntPtr GetInternalProfilesOnWeakInternal(System.IntPtr x0);

        public static Profiles GetInternalProfilesOnWeak(Face x0)
        {
            System.IntPtr temp_object_value = GetInternalProfilesOnWeakInternal(x0.__Ptr);
            Profiles temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Profiles();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetTexts@4")]
        private static extern System.IntPtr GetTextsInternal(System.IntPtr x0);

        public static Texts GetTexts(FacePtr x0)
        {
            System.IntPtr temp_object_value = GetTextsInternal(x0.__Ptr);
            Texts temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Texts();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetTextsOnWeak@4")]
        private static extern System.IntPtr GetTextsOnWeakInternal(System.IntPtr x0);

        public static Texts GetTextsOnWeak(Face x0)
        {
            System.IntPtr temp_object_value = GetTextsOnWeakInternal(x0.__Ptr);
            Texts temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Texts();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewEdgeIterator@4")]
        private static extern System.IntPtr NewEdgeIteratorInternal(System.IntPtr x0);

        public static EdgeIteratorPtr NewEdgeIterator(ProfilePtr x0)
        {
            System.IntPtr temp_object_value = NewEdgeIteratorInternal(x0.__Ptr);
            EdgeIteratorPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new EdgeIteratorPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewEdgeIteratorOnWeak@4")]
        private static extern System.IntPtr NewEdgeIteratorOnWeakInternal(System.IntPtr x0);

        public static EdgeIteratorPtr NewEdgeIteratorOnWeak(Profile x0)
        {
            System.IntPtr temp_object_value = NewEdgeIteratorOnWeakInternal(x0.__Ptr);
            EdgeIteratorPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new EdgeIteratorPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteEdgeIterator@4")]
        private static extern void DeleteEdgeIteratorInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dEdgeIteratorHasNext@4")]
        private static extern bool EdgeIteratorHasNextInternal(System.IntPtr x0);

        public static bool EdgeIteratorHasNext(EdgeIteratorPtr x0)
        {
            bool returned_value = EdgeIteratorHasNextInternal(x0.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dEdgeIteratorNext@4")]
        private static extern System.IntPtr EdgeIteratorNextInternal(System.IntPtr x0);

        public static Edge EdgeIteratorNext(EdgeIteratorPtr x0)
        {
            System.IntPtr temp_object_value = EdgeIteratorNextInternal(x0.__Ptr);
            Edge temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Edge();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetEdgeType@4")]
        private static extern EdgeType GetEdgeTypeInternal(System.IntPtr x0);

        public static EdgeType GetEdgeType(Edge x0)
        {
            EdgeType returned_value = GetEdgeTypeInternal(x0.__Ptr);
            return returned_value;
        }

        // Topo2dGetSegment is not a simple function.

        // Topo2dGetArc is not a simple function.

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetPreviousVertex@4")]
        private static extern System.IntPtr GetPreviousVertexInternal(System.IntPtr edge);

        public static Vertex GetPreviousVertex(Edge edge)
        {
            System.IntPtr temp_object_value = GetPreviousVertexInternal(edge.__Ptr);
            Vertex temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Vertex();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetNextVertex@4")]
        private static extern System.IntPtr GetNextVertexInternal(System.IntPtr edge);

        public static Vertex GetNextVertex(Edge edge)
        {
            System.IntPtr temp_object_value = GetNextVertexInternal(edge.__Ptr);
            Vertex temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Vertex();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        // Topo2dGetPosition is not a simple function.

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertProfile@4")]
        private static extern System.IntPtr ConvertProfileInternal(System.IntPtr x0);

        public static Profile ConvertProfile(ProfilePtr x0)
        {
            System.IntPtr temp_object_value = ConvertProfileInternal(x0.__Ptr);
            Profile temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Profile();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertProfiles@4")]
        private static extern System.IntPtr ConvertProfilesInternal(System.IntPtr x0);

        public static Profiles ConvertProfiles(ProfilesPtr x0)
        {
            System.IntPtr temp_object_value = ConvertProfilesInternal(x0.__Ptr);
            Profiles temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Profiles();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertFace@4")]
        private static extern System.IntPtr ConvertFaceInternal(System.IntPtr x0);

        public static Face ConvertFace(FacePtr x0)
        {
            System.IntPtr temp_object_value = ConvertFaceInternal(x0.__Ptr);
            Face temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Face();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertFaces@4")]
        private static extern System.IntPtr ConvertFacesInternal(System.IntPtr faces);

        public static Faces ConvertFaces(FacesPtr faces)
        {
            System.IntPtr temp_object_value = ConvertFacesInternal(faces.__Ptr);
            Faces temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Faces();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertPart@4")]
        private static extern System.IntPtr ConvertPartInternal(System.IntPtr x0);

        public static Part ConvertPart(PartPtr x0)
        {
            System.IntPtr temp_object_value = ConvertPartInternal(x0.__Ptr);
            Part temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Part();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertParts@4")]
        private static extern System.IntPtr ConvertPartsInternal(System.IntPtr parts);

        public static Parts ConvertParts(PartsPtr parts)
        {
            System.IntPtr temp_object_value = ConvertPartsInternal(parts.__Ptr);
            Parts temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Parts();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertText@4")]
        private static extern System.IntPtr ConvertTextInternal(System.IntPtr x0);

        public static Text ConvertText(TextPtr x0)
        {
            System.IntPtr temp_object_value = ConvertTextInternal(x0.__Ptr);
            Text temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Text();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertTexts@4")]
        private static extern System.IntPtr ConvertTextsInternal(System.IntPtr x0);

        public static Texts ConvertTexts(TextsPtr x0)
        {
            System.IntPtr temp_object_value = ConvertTextsInternal(x0.__Ptr);
            Texts temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Texts();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewParts@0")]
        private static extern System.IntPtr NewPartsInternal();

        public static PartsPtr NewParts()
        {
            System.IntPtr temp_object_value = NewPartsInternal();
            PartsPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new PartsPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteParts@4")]
        private static extern void DeletePartsInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dInsertPartPtr@8")]
        private static extern void InsertPartPtrInternal(System.IntPtr x0, System.IntPtr x1);

        public static void InsertPartPtr(PartsPtr x0, PartPtr x1)
        {
            InsertPartPtrInternal(x0.__Ptr, x1.__Ptr);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewFaces@0")]
        private static extern System.IntPtr NewFacesInternal();

        public static FacesPtr NewFaces()
        {
            System.IntPtr temp_object_value = NewFacesInternal();
            FacesPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacesPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteFaces@4")]
        private static extern void DeleteFacesInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dInsertFacePtr@8")]
        private static extern void InsertFacePtrInternal(System.IntPtr x0, System.IntPtr x1);

        public static void InsertFacePtr(FacesPtr x0, FacePtr x1)
        {
            InsertFacePtrInternal(x0.__Ptr, x1.__Ptr);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewProfiles@0")]
        private static extern System.IntPtr NewProfilesInternal();

        public static ProfilesPtr NewProfiles()
        {
            System.IntPtr temp_object_value = NewProfilesInternal();
            ProfilesPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new ProfilesPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteProfiles@4")]
        private static extern void DeleteProfilesInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dInsertProfilePtr@8")]
        private static extern void InsertProfilePtrInternal(System.IntPtr x0, System.IntPtr x1);

        public static void InsertProfilePtr(ProfilesPtr x0, ProfilePtr x1)
        {
            InsertProfilePtrInternal(x0.__Ptr, x1.__Ptr);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewTexts@0")]
        private static extern System.IntPtr NewTextsInternal();

        public static TextsPtr NewTexts()
        {
            System.IntPtr temp_object_value = NewTextsInternal();
            TextsPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new TextsPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteTexts@4")]
        private static extern void DeleteTextsInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dInsertTextPtr@8")]
        private static extern void InsertTextPtrInternal(System.IntPtr texts, System.IntPtr text);

        public static void InsertTextPtr(TextsPtr texts, TextPtr text)
        {
            InsertTextPtrInternal(texts.__Ptr, text.__Ptr);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewPartIterator@4")]
        private static extern System.IntPtr NewPartIteratorInternal(System.IntPtr x0);

        public static PartIteratorPtr NewPartIterator(Parts x0)
        {
            System.IntPtr temp_object_value = NewPartIteratorInternal(x0.__Ptr);
            PartIteratorPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new PartIteratorPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeletePartIterator@4")]
        private static extern void DeletePartIteratorInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dPartIteratorHasNext@4")]
        private static extern bool PartIteratorHasNextInternal(System.IntPtr x0);

        public static bool PartIteratorHasNext(PartIteratorPtr x0)
        {
            bool returned_value = PartIteratorHasNextInternal(x0.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dPartIteratorNext@4")]
        private static extern System.IntPtr PartIteratorNextInternal(System.IntPtr x0);

        public static Part PartIteratorNext(PartIteratorPtr x0)
        {
            System.IntPtr temp_object_value = PartIteratorNextInternal(x0.__Ptr);
            Part temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Part();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewFaceIterator@4")]
        private static extern System.IntPtr NewFaceIteratorInternal(System.IntPtr x0);

        public static FaceIteratorPtr NewFaceIterator(Faces x0)
        {
            System.IntPtr temp_object_value = NewFaceIteratorInternal(x0.__Ptr);
            FaceIteratorPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FaceIteratorPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteFaceIterator@4")]
        private static extern void DeleteFaceIteratorInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFaceIteratorHasNext@4")]
        private static extern bool FaceIteratorHasNextInternal(System.IntPtr x0);

        public static bool FaceIteratorHasNext(FaceIteratorPtr x0)
        {
            bool returned_value = FaceIteratorHasNextInternal(x0.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFaceIteratorNext@4")]
        private static extern System.IntPtr FaceIteratorNextInternal(System.IntPtr x0);

        public static Face FaceIteratorNext(FaceIteratorPtr x0)
        {
            System.IntPtr temp_object_value = FaceIteratorNextInternal(x0.__Ptr);
            Face temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Face();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewProfileIterator@4")]
        private static extern System.IntPtr NewProfileIteratorInternal(System.IntPtr x0);

        public static ProfileIteratorPtr NewProfileIterator(Profiles x0)
        {
            System.IntPtr temp_object_value = NewProfileIteratorInternal(x0.__Ptr);
            ProfileIteratorPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new ProfileIteratorPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteProfileIterator@4")]
        private static extern void DeleteProfileIteratorInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileIteratorHasNext@4")]
        private static extern bool ProfileIteratorHasNextInternal(System.IntPtr x0);

        public static bool ProfileIteratorHasNext(ProfileIteratorPtr x0)
        {
            bool returned_value = ProfileIteratorHasNextInternal(x0.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileIteratorNext@4")]
        private static extern System.IntPtr ProfileIteratorNextInternal(System.IntPtr x0);

        public static Profile ProfileIteratorNext(ProfileIteratorPtr x0)
        {
            System.IntPtr temp_object_value = ProfileIteratorNextInternal(x0.__Ptr);
            Profile temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Profile();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewTextIterator@4")]
        private static extern System.IntPtr NewTextIteratorInternal(System.IntPtr x0);

        public static TextIteratorPtr NewTextIterator(Texts x0)
        {
            System.IntPtr temp_object_value = NewTextIteratorInternal(x0.__Ptr);
            TextIteratorPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new TextIteratorPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteTextIterator@4")]
        private static extern void DeleteTextIteratorInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTextIteratorHasNext@4")]
        private static extern bool TextIteratorHasNextInternal(System.IntPtr x0);

        public static bool TextIteratorHasNext(TextIteratorPtr x0)
        {
            bool returned_value = TextIteratorHasNextInternal(x0.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTextIteratorNext@4")]
        private static extern System.IntPtr TextIteratorNextInternal(System.IntPtr x0);

        public static Text TextIteratorNext(TextIteratorPtr x0)
        {
            System.IntPtr temp_object_value = TextIteratorNextInternal(x0.__Ptr);
            Text temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Text();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWriteProfileP01@8")]
        private static extern void WriteProfileP01Internal(System.IntPtr profile, string filename);

        public static void WriteProfileP01(Profile profile, string filename)
        {
            WriteProfileP01Internal(profile.__Ptr, filename);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWriteFaceP01@8")]
        private static extern void WriteFaceP01Internal(System.IntPtr face, string filename);

        public static void WriteFaceP01(Face face, string filename)
        {
            WriteFaceP01Internal(face.__Ptr, filename);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWritePartP01@8")]
        private static extern void WritePartP01Internal(System.IntPtr part, string filename);

        public static void WritePartP01(Part part, string filename)
        {
            WritePartP01Internal(part.__Ptr, filename);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadPartPtrP01@4")]
        private static extern System.IntPtr ReadPartPtrP01Internal(string filename);

        public static PartPtr ReadPartPtrP01(string filename)
        {
            System.IntPtr temp_object_value = ReadPartPtrP01Internal(filename);
            PartPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new PartPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadProfilesPtrP01@4")]
        private static extern System.IntPtr ReadProfilesPtrP01Internal(string filename);

        public static ProfilesPtr ReadProfilesPtrP01(string filename)
        {
            System.IntPtr temp_object_value = ReadProfilesPtrP01Internal(filename);
            ProfilesPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new ProfilesPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadProfilesPtrDpr@4")]
        private static extern System.IntPtr ReadProfilesPtrDprInternal(string filename);

        public static ProfilesPtr ReadProfilesPtrDpr(string filename)
        {
            System.IntPtr temp_object_value = ReadProfilesPtrDprInternal(filename);
            ProfilesPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new ProfilesPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadTextsPtrDpr@4")]
        private static extern System.IntPtr ReadTextsPtrDprInternal(string filename);

        public static TextsPtr ReadTextsPtrDpr(string filename)
        {
            System.IntPtr temp_object_value = ReadTextsPtrDprInternal(filename);
            TextsPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new TextsPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadFacePtrDpr@4")]
        private static extern System.IntPtr ReadFacePtrDprInternal(string filename);

        public static FacePtr ReadFacePtrDpr(string filename)
        {
            System.IntPtr temp_object_value = ReadFacePtrDprInternal(filename);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadPartPtrDpr@4")]
        private static extern System.IntPtr ReadPartPtrDprInternal(string filename);

        public static PartPtr ReadPartPtrDpr(string filename)
        {
            System.IntPtr temp_object_value = ReadPartPtrDprInternal(filename);
            PartPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new PartPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadCutPartPtrDpr@4")]
        private static extern System.IntPtr ReadCutPartPtrDprInternal(string filename);

        public static PartPtr ReadCutPartPtrDpr(string filename)
        {
            System.IntPtr temp_object_value = ReadCutPartPtrDprInternal(filename);
            PartPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new PartPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadDprAttribute@4")]
        private static extern System.IntPtr ReadDprAttributeInternal(string filename);

        public static DprAttributePtr ReadDprAttribute(string filename)
        {
            System.IntPtr temp_object_value = ReadDprAttributeInternal(filename);
            DprAttributePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new DprAttributePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWriteProfileDpr@8")]
        private static extern void WriteProfileDprInternal(System.IntPtr profile, string filename);

        public static void WriteProfileDpr(Profile profile, string filename)
        {
            WriteProfileDprInternal(profile.__Ptr, filename);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWriteProfilesDpr@8")]
        private static extern void WriteProfilesDprInternal(System.IntPtr profiles, string filename);

        public static void WriteProfilesDpr(Profiles profiles, string filename)
        {
            WriteProfilesDprInternal(profiles.__Ptr, filename);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWriteFaceDpr@8")]
        private static extern void WriteFaceDprInternal(System.IntPtr face, string filename);

        public static void WriteFaceDpr(Face face, string filename)
        {
            WriteFaceDprInternal(face.__Ptr, filename);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWritePartDpr@8")]
        private static extern void WritePartDprInternal(System.IntPtr part, string filename);

        public static void WritePartDpr(Part part, string filename)
        {
            WritePartDprInternal(part.__Ptr, filename);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterNew@4")]
        private static extern System.IntPtr DprWriterNewInternal(string filename);

        public static DprWriterPtr DprWriterNew(string filename)
        {
            System.IntPtr temp_object_value = DprWriterNewInternal(filename);
            DprWriterPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new DprWriterPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterDelete@4")]
        private static extern void DprWriterDeleteInternal(System.IntPtr writer);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterSetDprAttribute@8")]
        private static extern void DprWriterSetDprAttributeInternal(System.IntPtr writer, System.IntPtr dpr_attribute);

        public static void DprWriterSetDprAttribute(DprWriterPtr writer, DprAttributePtr dpr_attribute)
        {
            DprWriterSetDprAttributeInternal(writer.__Ptr, dpr_attribute.__Ptr);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterSetOrientation@8")]
        private static extern void DprWriterSetOrientationInternal(System.IntPtr writer, bool material_left);

        public static void DprWriterSetOrientation(DprWriterPtr writer, bool material_left)
        {
            DprWriterSetOrientationInternal(writer.__Ptr, material_left);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterWriteProfile@8")]
        private static extern int DprWriterWriteProfileInternal(System.IntPtr writer, System.IntPtr profile);

        public static int DprWriterWriteProfile(DprWriterPtr writer, Profile profile)
        {
            int returned_value = DprWriterWriteProfileInternal(writer.__Ptr, profile.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterWriteProfiles@8")]
        private static extern int DprWriterWriteProfilesInternal(System.IntPtr writer, System.IntPtr profiles);

        public static int DprWriterWriteProfiles(DprWriterPtr writer, Profiles profiles)
        {
            int returned_value = DprWriterWriteProfilesInternal(writer.__Ptr, profiles.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterWriteFace@8")]
        private static extern int DprWriterWriteFaceInternal(System.IntPtr writer, System.IntPtr face);

        public static int DprWriterWriteFace(DprWriterPtr writer, Face face)
        {
            int returned_value = DprWriterWriteFaceInternal(writer.__Ptr, face.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterWritePart@8")]
        private static extern int DprWriterWritePartInternal(System.IntPtr writer, System.IntPtr part);

        public static int DprWriterWritePart(DprWriterPtr writer, Part part)
        {
            int returned_value = DprWriterWritePartInternal(writer.__Ptr, part.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWriteProfile@8")]
        private static extern void WriteProfileInternal(System.IntPtr profile, string filename);

        public static void WriteProfile(Profile profile, string filename)
        {
            WriteProfileInternal(profile.__Ptr, filename);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadProfile@4")]
        private static extern System.IntPtr ReadProfileInternal(string filename);

        public static ProfilePtr ReadProfile(string filename)
        {
            System.IntPtr temp_object_value = ReadProfileInternal(filename);
            ProfilePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new ProfilePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWritePart@8")]
        private static extern void WritePartInternal(System.IntPtr part, string filename);

        public static void WritePart(Part part, string filename)
        {
            WritePartInternal(part.__Ptr, filename);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadPart@4")]
        private static extern System.IntPtr ReadPartInternal(string filename);

        public static PartPtr ReadPart(string filename)
        {
            System.IntPtr temp_object_value = ReadPartInternal(filename);
            PartPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new PartPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWritePartString@16")]
        private static extern int WritePartStringInternal(System.IntPtr part, string buffer, int buffer_size, out int actual_size);

        public static int WritePartString(Part part, string buffer, int buffer_size, out int actual_size)
        {
            int returned_value = WritePartStringInternal(part.__Ptr, buffer, buffer_size, out actual_size);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadPartString@8")]
        private static extern System.IntPtr ReadPartStringInternal(string buffer, int buffer_size);

        public static PartPtr ReadPartString(string buffer, int buffer_size)
        {
            System.IntPtr temp_object_value = ReadPartStringInternal(buffer, buffer_size);
            PartPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new PartPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewProfileXY@16")]
        private static extern System.IntPtr NewProfileXYInternal(double x, double y);

        public static ProfilePtr NewProfileXY(double x, double y)
        {
            System.IntPtr temp_object_value = NewProfileXYInternal(x, y);
            ProfilePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new ProfilePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dAddSegmentXY@20")]
        private static extern System.IntPtr AddSegmentXYInternal(System.IntPtr profile, double x, double y);

        public static Edge AddSegmentXY(ProfilePtr profile, double x, double y)
        {
            System.IntPtr temp_object_value = AddSegmentXYInternal(profile.__Ptr, x, y);
            Edge temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Edge();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dAddArcXY@40")]
        private static extern System.IntPtr AddArcXYInternal(System.IntPtr profile, double x, double y, double x_center, double y_center, bool trigo);

        public static Edge AddArcXY(ProfilePtr profile, double x, double y, double x_center, double y_center, bool trigo)
        {
            System.IntPtr temp_object_value = AddArcXYInternal(profile.__Ptr, x, y, x_center, y_center, trigo);
            Edge temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Edge();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dAddArcXYArrow@28")]
        private static extern System.IntPtr AddArcXYArrowInternal(System.IntPtr profile, double x, double y, double arrow);

        public static Edge AddArcXYArrow(ProfilePtr profile, double x, double y, double arrow)
        {
            System.IntPtr temp_object_value = AddArcXYArrowInternal(profile.__Ptr, x, y, arrow);
            Edge temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new Edge();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetSegmentXY@20")]
        private static extern void GetSegmentXYInternal(System.IntPtr edge, out double start_x, out double start_y, out double end_x, out double end_y);

        public static void GetSegmentXY(Edge edge, out double start_x, out double start_y, out double end_x, out double end_y)
        {
            GetSegmentXYInternal(edge.__Ptr, out start_x, out start_y, out end_x, out end_y);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetArcXY@32")]
        private static extern void GetArcXYInternal(System.IntPtr edge, out double start_x, out double start_y, out double end_x, out double end_y, out double center_x, out double center_y, out bool trigo);

        public static void GetArcXY(Edge edge, out double start_x, out double start_y, out double end_x, out double end_y, out double center_x, out double center_y, out bool trigo)
        {
            GetArcXYInternal(edge.__Ptr, out start_x, out start_y, out end_x, out end_y, out center_x, out center_y, out trigo);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetPartBoundingBox@20")]
        private static extern void GetPartBoundingBoxInternal(System.IntPtr part, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y);

        public static void GetPartBoundingBox(Part part, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y)
        {
            GetPartBoundingBoxInternal(part.__Ptr, out bottom_left_x, out bottom_left_y, out top_right_x, out top_right_y);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetFaceBoundingBox@20")]
        private static extern void GetFaceBoundingBoxInternal(System.IntPtr face, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y);

        public static void GetFaceBoundingBox(Face face, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y)
        {
            GetFaceBoundingBoxInternal(face.__Ptr, out bottom_left_x, out bottom_left_y, out top_right_x, out top_right_y);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetProfileBoundingBox@20")]
        private static extern void GetProfileBoundingBoxInternal(System.IntPtr profile, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y);

        public static void GetProfileBoundingBox(Profile profile, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y)
        {
            GetProfileBoundingBoxInternal(profile.__Ptr, out bottom_left_x, out bottom_left_y, out top_right_x, out top_right_y);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetProfilesBoundingBox@20")]
        private static extern void GetProfilesBoundingBoxInternal(System.IntPtr profile, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y);

        public static void GetProfilesBoundingBox(Profiles profile, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y)
        {
            GetProfilesBoundingBoxInternal(profile.__Ptr, out bottom_left_x, out bottom_left_y, out top_right_x, out top_right_y);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetTextBoundingBox@20")]
        private static extern void GetTextBoundingBoxInternal(System.IntPtr text, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y);

        public static void GetTextBoundingBox(Text text, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y)
        {
            GetTextBoundingBoxInternal(text.__Ptr, out bottom_left_x, out bottom_left_y, out top_right_x, out top_right_y);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetFaceMinSize@12")]
        private static extern void GetFaceMinSizeInternal(System.IntPtr face, out double length, out double width);

        public static void GetFaceMinSize(Face face, out double length, out double width)
        {
            GetFaceMinSizeInternal(face.__Ptr, out length, out width);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetFaceArea@4")]
        private static extern double GetFaceAreaInternal(System.IntPtr face);

        public static double GetFaceArea(Face face)
        {
            double returned_value = GetFaceAreaInternal(face.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetPartArea@4")]
        private static extern double GetPartAreaInternal(System.IntPtr part);

        public static double GetPartArea(Part part)
        {
            double returned_value = GetPartAreaInternal(part.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetProfileLength@4")]
        private static extern double GetProfileLengthInternal(System.IntPtr profile);

        public static double GetProfileLength(Profile profile)
        {
            double returned_value = GetProfileLengthInternal(profile.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetProfileIsClosed@4")]
        private static extern bool GetProfileIsClosedInternal(System.IntPtr profile);

        public static bool GetProfileIsClosed(Profile profile)
        {
            bool returned_value = GetProfileIsClosedInternal(profile.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetProfileArea@4")]
        private static extern double GetProfileAreaInternal(System.IntPtr profile);

        public static double GetProfileArea(Profile profile)
        {
            double returned_value = GetProfileAreaInternal(profile.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetEdgeLength@4")]
        private static extern double GetEdgeLengthInternal(System.IntPtr edge);

        public static double GetEdgeLength(Edge edge)
        {
            double returned_value = GetEdgeLengthInternal(edge.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dIdentifyPartWithRectangularExternal@4")]
        private static extern bool IdentifyPartWithRectangularExternalInternal(System.IntPtr part);

        public static bool IdentifyPartWithRectangularExternal(Part part)
        {
            bool returned_value = IdentifyPartWithRectangularExternalInternal(part.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dRotatePart@28")]
        private static extern void RotatePartInternal(System.IntPtr part, double angle, double pivot_x, double pivot_y);

        public static void RotatePart(PartPtr part, double angle, double pivot_x, double pivot_y)
        {
            RotatePartInternal(part.__Ptr, angle, pivot_x, pivot_y);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dRotateFace@28")]
        private static extern void RotateFaceInternal(System.IntPtr face, double angle, double pivot_x, double pivot_y);

        public static void RotateFace(FacePtr face, double angle, double pivot_x, double pivot_y)
        {
            RotateFaceInternal(face.__Ptr, angle, pivot_x, pivot_y);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dRotateProfile@28")]
        private static extern void RotateProfileInternal(System.IntPtr profile, double angle, double pivot_x, double pivot_y);

        public static void RotateProfile(ProfilePtr profile, double angle, double pivot_x, double pivot_y)
        {
            RotateProfileInternal(profile.__Ptr, angle, pivot_x, pivot_y);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dRotateText@28")]
        private static extern void RotateTextInternal(System.IntPtr text, double angle, double pivot_x, double pivot_y);

        public static void RotateText(TextPtr text, double angle, double pivot_x, double pivot_y)
        {
            RotateTextInternal(text.__Ptr, angle, pivot_x, pivot_y);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTranslatePart@20")]
        private static extern void TranslatePartInternal(System.IntPtr part, double x_move, double y_move);

        public static void TranslatePart(PartPtr part, double x_move, double y_move)
        {
            TranslatePartInternal(part.__Ptr, x_move, y_move);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTranslateFace@20")]
        private static extern void TranslateFaceInternal(System.IntPtr face, double x_move, double y_move);

        public static void TranslateFace(FacePtr face, double x_move, double y_move)
        {
            TranslateFaceInternal(face.__Ptr, x_move, y_move);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTranslateProfile@20")]
        private static extern void TranslateProfileInternal(System.IntPtr profile, double x_move, double y_move);

        public static void TranslateProfile(ProfilePtr profile, double x_move, double y_move)
        {
            TranslateProfileInternal(profile.__Ptr, x_move, y_move);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTranslateText@20")]
        private static extern void TranslateTextInternal(System.IntPtr text, double x_move, double y_move);

        public static void TranslateText(TextPtr text, double x_move, double y_move)
        {
            TranslateTextInternal(text.__Ptr, x_move, y_move);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSymmetrizePart@36")]
        private static extern void SymmetrizePartInternal(System.IntPtr part, double x_base, double y_base, double x_vector, double y_vector);

        public static void SymmetrizePart(PartPtr part, double x_base, double y_base, double x_vector, double y_vector)
        {
            SymmetrizePartInternal(part.__Ptr, x_base, y_base, x_vector, y_vector);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSymmetrizeFace@36")]
        private static extern void SymmetrizeFaceInternal(System.IntPtr face, double x_base, double y_base, double x_vector, double y_vector);

        public static void SymmetrizeFace(FacePtr face, double x_base, double y_base, double x_vector, double y_vector)
        {
            SymmetrizeFaceInternal(face.__Ptr, x_base, y_base, x_vector, y_vector);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSymmetrizeProfile@36")]
        private static extern void SymmetrizeProfileInternal(System.IntPtr profile, double x_base, double y_base, double x_vector, double y_vector);

        public static void SymmetrizeProfile(ProfilePtr profile, double x_base, double y_base, double x_vector, double y_vector)
        {
            SymmetrizeProfileInternal(profile.__Ptr, x_base, y_base, x_vector, y_vector);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSymmetrizeText@36")]
        private static extern void SymmetrizeTextInternal(System.IntPtr text, double x_base, double y_base, double x_vector, double y_vector);

        public static void SymmetrizeText(TextPtr text, double x_base, double y_base, double x_vector, double y_vector)
        {
            SymmetrizeTextInternal(text.__Ptr, x_base, y_base, x_vector, y_vector);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTranslateBBBottomLeftToOrigin@4")]
        private static extern void TranslateBBBottomLeftToOriginInternal(System.IntPtr profile);

        public static void TranslateBBBottomLeftToOrigin(ProfilePtr profile)
        {
            TranslateBBBottomLeftToOriginInternal(profile.__Ptr);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileCornerBottomLeft@4")]
        private static extern int ProfileCornerBottomLeftInternal(System.IntPtr profile);

        public static int ProfileCornerBottomLeft(ProfilePtr profile)
        {
            int returned_value = ProfileCornerBottomLeftInternal(profile.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileRightAngleAtCorner@8")]
        private static extern int ProfileRightAngleAtCornerInternal(System.IntPtr profile, int wanted_corner);

        public static int ProfileRightAngleAtCorner(ProfilePtr profile, int wanted_corner)
        {
            int returned_value = ProfileRightAngleAtCornerInternal(profile.__Ptr, wanted_corner);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFilletXY@28")]
        private static extern bool FilletXYInternal(System.IntPtr profile, double x, double y, double radius);

        public static bool FilletXY(ProfilePtr profile, double x, double y, double radius)
        {
            bool returned_value = FilletXYInternal(profile.__Ptr, x, y, radius);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFilletAll@28")]
        private static extern void FilletAllInternal(System.IntPtr profile, double radius, double minThreshold, double maxThreshold);

        public static void FilletAll(ProfilePtr profile, double radius, double minThreshold, double maxThreshold)
        {
            FilletAllInternal(profile.__Ptr, radius, minThreshold, maxThreshold);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dIsInsideProfile@32")]
        private static extern int IsInsideProfileInternal(System.IntPtr profile, double point_x, double point_y, int boundary_accepted, double max_distance);

        public static int IsInsideProfile(ProfilePtr profile, double point_x, double point_y, int boundary_accepted, double max_distance)
        {
            int returned_value = IsInsideProfileInternal(profile.__Ptr, point_x, point_y, boundary_accepted, max_distance);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dIsInsideFace@36")]
        private static extern int IsInsideFaceInternal(System.IntPtr face, double point_x, double point_y, int external_boundary_accepted, int internal_boundary_accepted, double max_distance);

        public static int IsInsideFace(FacePtr face, double point_x, double point_y, int external_boundary_accepted, int internal_boundary_accepted, double max_distance)
        {
            int returned_value = IsInsideFaceInternal(face.__Ptr, point_x, point_y, external_boundary_accepted, internal_boundary_accepted, max_distance);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfilesIntersect@8")]
        private static extern bool ProfilesIntersectInternal(System.IntPtr profile1, System.IntPtr profile2);

        public static bool ProfilesIntersect(ProfilePtr profile1, ProfilePtr profile2)
        {
            bool returned_value = ProfilesIntersectInternal(profile1.__Ptr, profile2.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileSplitOnIntersections@4")]
        private static extern System.IntPtr ProfileSplitOnIntersectionsInternal(System.IntPtr profile);

        public static ProfilesPtr ProfileSplitOnIntersections(ProfilePtr profile)
        {
            System.IntPtr temp_object_value = ProfileSplitOnIntersectionsInternal(profile.__Ptr);
            ProfilesPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new ProfilesPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGridCutting@40")]
        private static extern System.IntPtr GridCuttingInternal(System.IntPtr part, double dx, double dy, double leadin_distance, double border_distance, bool smallest_side_first);

        public static ProfilesPtr GridCutting(Part part, double dx, double dy, double leadin_distance, double border_distance, bool smallest_side_first)
        {
            System.IntPtr temp_object_value = GridCuttingInternal(part.__Ptr, dx, dy, leadin_distance, border_distance, smallest_side_first);
            ProfilesPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new ProfilesPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGridCuttingMinLengthLines@40")]
        private static extern System.IntPtr GridCuttingMinLengthLinesInternal(System.IntPtr part, double dx, double dy, double leadin_distance, double border_distance, bool smallest_side_first);

        public static ProfilesPtr GridCuttingMinLengthLines(Part part, double dx, double dy, double leadin_distance, double border_distance, bool smallest_side_first)
        {
            System.IntPtr temp_object_value = GridCuttingMinLengthLinesInternal(part.__Ptr, dx, dy, leadin_distance, border_distance, smallest_side_first);
            ProfilesPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new ProfilesPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileFindArcs@12")]
        private static extern void ProfileFindArcsInternal(System.IntPtr profile, double epsilon);

        public static void ProfileFindArcs(ProfilePtr profile, double epsilon)
        {
            ProfileFindArcsInternal(profile.__Ptr, epsilon);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeCircleFaceXY@8")]
        private static extern System.IntPtr MakeCircleFaceXYInternal(double radius);

        public static FacePtr MakeCircleFaceXY(double radius)
        {
            System.IntPtr temp_object_value = MakeCircleFaceXYInternal(radius);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakePolygonEllipseFace@20")]
        private static extern System.IntPtr MakePolygonEllipseFaceInternal(double big_radius, double small_radius, int nb_sides);

        public static FacePtr MakePolygonEllipseFace(double big_radius, double small_radius, int nb_sides)
        {
            System.IntPtr temp_object_value = MakePolygonEllipseFaceInternal(big_radius, small_radius, nb_sides);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeArcsEllipseProfile@20")]
        private static extern System.IntPtr MakeArcsEllipseProfileInternal(double big_radius, double small_radius, int arcs_number);

        public static ProfilePtr MakeArcsEllipseProfile(double big_radius, double small_radius, int arcs_number)
        {
            System.IntPtr temp_object_value = MakeArcsEllipseProfileInternal(big_radius, small_radius, arcs_number);
            ProfilePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new ProfilePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeCircleFaceWithPositionedCircleHoleXY@24")]
        private static extern System.IntPtr MakeCircleFaceWithPositionedCircleHoleXYInternal(double radius_ext, double radius_int, double offset);

        public static FacePtr MakeCircleFaceWithPositionedCircleHoleXY(double radius_ext, double radius_int, double offset)
        {
            System.IntPtr temp_object_value = MakeCircleFaceWithPositionedCircleHoleXYInternal(radius_ext, radius_int, offset);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeCircleFaceWithCenteredRingOfHoleXY@36")]
        private static extern System.IntPtr MakeCircleFaceWithCenteredRingOfHoleXYInternal(double radius_ext, double radius_ring, double radius_hole, uint N, double radius_int);

        public static FacePtr MakeCircleFaceWithCenteredRingOfHoleXY(double radius_ext, double radius_ring, double radius_hole, uint N, double radius_int)
        {
            System.IntPtr temp_object_value = MakeCircleFaceWithCenteredRingOfHoleXYInternal(radius_ext, radius_ring, radius_hole, N, radius_int);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeCircleFaceWith2CenteredRingsOfHoleXY@80")]
        private static extern System.IntPtr MakeCircleFaceWith2CenteredRingsOfHoleXYInternal(double radius_ext, double radius_int, double radius_ring1, double radius_hole1, double theta, uint N1, double radius_ring2, double radius_hole2, double angle_start2, double angle_move2, uint N2);

        public static FacePtr MakeCircleFaceWith2CenteredRingsOfHoleXY(double radius_ext, double radius_int, double radius_ring1, double radius_hole1, double theta, uint N1, double radius_ring2, double radius_hole2, double angle_start2, double angle_move2, uint N2)
        {
            System.IntPtr temp_object_value = MakeCircleFaceWith2CenteredRingsOfHoleXYInternal(radius_ext, radius_int, radius_ring1, radius_hole1, theta, N1, radius_ring2, radius_hole2, angle_start2, angle_move2, N2);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeBrokenRingFaceWith2CenteredRingsOfHoleXY@96")]
        private static extern System.IntPtr MakeBrokenRingFaceWith2CenteredRingsOfHoleXYInternal(double radius_ext, double radius_int, double theta, double radius_ring1, double radius_hole1, double angle_start1, double angle_move1, uint N1, double radius_ring2, double radius_hole2, double angle_start2, double angle_move2, uint N2);

        public static FacePtr MakeBrokenRingFaceWith2CenteredRingsOfHoleXY(double radius_ext, double radius_int, double theta, double radius_ring1, double radius_hole1, double angle_start1, double angle_move1, uint N1, double radius_ring2, double radius_hole2, double angle_start2, double angle_move2, uint N2)
        {
            System.IntPtr temp_object_value = MakeBrokenRingFaceWith2CenteredRingsOfHoleXYInternal(radius_ext, radius_int, theta, radius_ring1, radius_hole1, angle_start1, angle_move1, N1, radius_ring2, radius_hole2, angle_start2, angle_move2, N2);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeBrokenRingFaceXY@24")]
        private static extern System.IntPtr MakeBrokenRingFaceXYInternal(double radius_ext, double radius_int, double theta);

        public static FacePtr MakeBrokenRingFaceXY(double radius_ext, double radius_int, double theta)
        {
            System.IntPtr temp_object_value = MakeBrokenRingFaceXYInternal(radius_ext, radius_int, theta);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeTriangleFaceXY@48")]
        private static extern System.IntPtr MakeTriangleFaceXYInternal(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y);

        public static FacePtr MakeTriangleFaceXY(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y)
        {
            System.IntPtr temp_object_value = MakeTriangleFaceXYInternal(p1_x, p1_y, p2_x, p2_y, p3_x, p3_y);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleProfileXY@32")]
        private static extern System.IntPtr MakeRectangleProfileXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y);

        public static ProfilePtr MakeRectangleProfileXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y)
        {
            System.IntPtr temp_object_value = MakeRectangleProfileXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y);
            ProfilePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new ProfilePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceXY@32")]
        private static extern System.IntPtr MakeRectangleFaceXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y);

        public static FacePtr MakeRectangleFaceXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y)
        {
            System.IntPtr temp_object_value = MakeRectangleFaceXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithHoleXY@40")]
        private static extern System.IntPtr MakeRectangleFaceWithHoleXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double radius);

        public static FacePtr MakeRectangleFaceWithHoleXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double radius)
        {
            System.IntPtr temp_object_value = MakeRectangleFaceWithHoleXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, radius);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithPositionedRectangleHoleXY@64")]
        private static extern System.IntPtr MakeRectangleFaceWithPositionedRectangleHoleXYInternal(double ext_bottom_left_x, double ext_bottom_left_y, double ext_top_right_x, double ext_top_right_y, double int_bottom_left_x, double int_bottom_left_y, double int_top_right_x, double int_top_right_y);

        public static FacePtr MakeRectangleFaceWithPositionedRectangleHoleXY(double ext_bottom_left_x, double ext_bottom_left_y, double ext_top_right_x, double ext_top_right_y, double int_bottom_left_x, double int_bottom_left_y, double int_top_right_x, double int_top_right_y)
        {
            System.IntPtr temp_object_value = MakeRectangleFaceWithPositionedRectangleHoleXYInternal(ext_bottom_left_x, ext_bottom_left_y, ext_top_right_x, ext_top_right_y, int_bottom_left_x, int_bottom_left_y, int_top_right_x, int_top_right_y);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithGridHoleXY@56")]
        private static extern System.IntPtr MakeRectangleFaceWithGridHoleXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, double dy, double radius);

        public static FacePtr MakeRectangleFaceWithGridHoleXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, double dy, double radius)
        {
            System.IntPtr temp_object_value = MakeRectangleFaceWithGridHoleXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, dx, dy, radius);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithGridNMHoleXY@64")]
        private static extern System.IntPtr MakeRectangleFaceWithGridNMHoleXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, double dy, int nX, int nY, double radius);

        public static FacePtr MakeRectangleFaceWithGridNMHoleXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, double dy, int nX, int nY, double radius)
        {
            System.IntPtr temp_object_value = MakeRectangleFaceWithGridNMHoleXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, dx, dy, nX, nY, radius);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithGridNMHoleOffsetXY@80")]
        private static extern System.IntPtr MakeRectangleFaceWithGridNMHoleOffsetXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double offset_x, double offset_y, double dX, double dY, int nX, int nY, double radius);

        public static FacePtr MakeRectangleFaceWithGridNMHoleOffsetXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double offset_x, double offset_y, double dX, double dY, int nX, int nY, double radius)
        {
            System.IntPtr temp_object_value = MakeRectangleFaceWithGridNMHoleOffsetXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, offset_x, offset_y, dX, dY, nX, nY, radius);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithOblongsAlignXY@56")]
        private static extern System.IntPtr MakeRectangleFaceWithOblongsAlignXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, double length, double radius);

        public static FacePtr MakeRectangleFaceWithOblongsAlignXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, double length, double radius)
        {
            System.IntPtr temp_object_value = MakeRectangleFaceWithOblongsAlignXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, dx, length, radius);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithNOblongsAlignXY@60")]
        private static extern System.IntPtr MakeRectangleFaceWithNOblongsAlignXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, int nX, double length, double radius);

        public static FacePtr MakeRectangleFaceWithNOblongsAlignXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, int nX, double length, double radius)
        {
            System.IntPtr temp_object_value = MakeRectangleFaceWithNOblongsAlignXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, dx, nX, length, radius);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithNOblongsAlignOffsetXY@76")]
        private static extern System.IntPtr MakeRectangleFaceWithNOblongsAlignOffsetXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double offset_x, double offset_y, double dX, int nX, double length, double radius);

        public static FacePtr MakeRectangleFaceWithNOblongsAlignOffsetXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double offset_x, double offset_y, double dX, int nX, double length, double radius)
        {
            System.IntPtr temp_object_value = MakeRectangleFaceWithNOblongsAlignOffsetXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, offset_x, offset_y, dX, nX, length, radius);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRoundRectangleFaceXY@40")]
        private static extern System.IntPtr MakeRoundRectangleFaceXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value);

        public static FacePtr MakeRoundRectangleFaceXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value)
        {
            System.IntPtr temp_object_value = MakeRoundRectangleFaceXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, round_value);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRoundRectangleFaceWithHoleXY@48")]
        private static extern System.IntPtr MakeRoundRectangleFaceWithHoleXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double radius);

        public static FacePtr MakeRoundRectangleFaceWithHoleXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double radius)
        {
            System.IntPtr temp_object_value = MakeRoundRectangleFaceWithHoleXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, round_value, radius);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRoundRectangleFaceWithOblongXY@56")]
        private static extern System.IntPtr MakeRoundRectangleFaceWithOblongXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double oblong_length, double oblong_radius);

        public static FacePtr MakeRoundRectangleFaceWithOblongXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double oblong_length, double oblong_radius)
        {
            System.IntPtr temp_object_value = MakeRoundRectangleFaceWithOblongXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, round_value, oblong_length, oblong_radius);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRoundRectangleFaceWithFiveHolesXY@72")]
        private static extern System.IntPtr MakeRoundRectangleFaceWithFiveHolesXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double radiusCorners, double radiusCenterHole, double dX, double dY, double radiusHolesInCorners);

        public static FacePtr MakeRoundRectangleFaceWithFiveHolesXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double radiusCorners, double radiusCenterHole, double dX, double dY, double radiusHolesInCorners)
        {
            System.IntPtr temp_object_value = MakeRoundRectangleFaceWithFiveHolesXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, radiusCorners, radiusCenterHole, dX, dY, radiusHolesInCorners);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeEatenRectangleFaceXY@40")]
        private static extern System.IntPtr MakeEatenRectangleFaceXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value);

        public static FacePtr MakeEatenRectangleFaceXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value)
        {
            System.IntPtr temp_object_value = MakeEatenRectangleFaceXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, round_value);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeEatenRectangleFaceWithHoleXY@48")]
        private static extern System.IntPtr MakeEatenRectangleFaceWithHoleXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double radius);

        public static FacePtr MakeEatenRectangleFaceWithHoleXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double radius)
        {
            System.IntPtr temp_object_value = MakeEatenRectangleFaceWithHoleXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, round_value, radius);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeEatenRectangleFaceWithOblongXY@56")]
        private static extern System.IntPtr MakeEatenRectangleFaceWithOblongXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double chamfer_value, double oblong_length, double oblong_radius);

        public static FacePtr MakeEatenRectangleFaceWithOblongXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double chamfer_value, double oblong_length, double oblong_radius)
        {
            System.IntPtr temp_object_value = MakeEatenRectangleFaceWithOblongXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, chamfer_value, oblong_length, oblong_radius);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeChamferRectangleFaceXY@40")]
        private static extern System.IntPtr MakeChamferRectangleFaceXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double chamfer_value);

        public static FacePtr MakeChamferRectangleFaceXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double chamfer_value)
        {
            System.IntPtr temp_object_value = MakeChamferRectangleFaceXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, chamfer_value);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeEatenTrapezoidXY@80")]
        private static extern System.IntPtr MakeEatenTrapezoidXYInternal(double bottom_left_x, double bottom_left_y, double bottom_right_x, double bottom_right_y, double top_left_x, double top_left_y, double top_right_x, double top_right_y, double top_offset, double radius);

        public static FacePtr MakeEatenTrapezoidXY(double bottom_left_x, double bottom_left_y, double bottom_right_x, double bottom_right_y, double top_left_x, double top_left_y, double top_right_x, double top_right_y, double top_offset, double radius)
        {
            System.IntPtr temp_object_value = MakeEatenTrapezoidXYInternal(bottom_left_x, bottom_left_y, bottom_right_x, bottom_right_y, top_left_x, top_left_y, top_right_x, top_right_y, top_offset, radius);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeChamferRectangleFaceWithHoleXY@48")]
        private static extern System.IntPtr MakeChamferRectangleFaceWithHoleXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double chamfer_value, double radius);

        public static FacePtr MakeChamferRectangleFaceWithHoleXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double chamfer_value, double radius)
        {
            System.IntPtr temp_object_value = MakeChamferRectangleFaceWithHoleXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, chamfer_value, radius);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeChamferRectangleFaceWithOblongXY@56")]
        private static extern System.IntPtr MakeChamferRectangleFaceWithOblongXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double oblong_length, double chamfer_value);

        public static FacePtr MakeChamferRectangleFaceWithOblongXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double oblong_length, double chamfer_value)
        {
            System.IntPtr temp_object_value = MakeChamferRectangleFaceWithOblongXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, round_value, oblong_length, chamfer_value);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeOblongFaceXY@32")]
        private static extern System.IntPtr MakeOblongFaceXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y);

        public static FacePtr MakeOblongFaceXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y)
        {
            System.IntPtr temp_object_value = MakeOblongFaceXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeLFaceXY@48")]
        private static extern System.IntPtr MakeLFaceXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double thicknessH, double thicknessV);

        public static FacePtr MakeLFaceXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double thicknessH, double thicknessV)
        {
            System.IntPtr temp_object_value = MakeLFaceXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, thicknessH, thicknessV);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeTFaceXY@32")]
        private static extern System.IntPtr MakeTFaceXYInternal(double length, double width, double thicknessH, double thicknessV);

        public static FacePtr MakeTFaceXY(double length, double width, double thicknessH, double thicknessV)
        {
            System.IntPtr temp_object_value = MakeTFaceXYInternal(length, width, thicknessH, thicknessV);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeCamXY@56")]
        private static extern System.IntPtr MakeCamXYInternal(double spacing_ext, double radius_ext_left, double radius_ext_right, double pos_int_left, double radius_int_left, double pos_int_right, double radius_int_right);

        public static FacePtr MakeCamXY(double spacing_ext, double radius_ext_left, double radius_ext_right, double pos_int_left, double radius_int_left, double pos_int_right, double radius_int_right)
        {
            System.IntPtr temp_object_value = MakeCamXYInternal(spacing_ext, radius_ext_left, radius_ext_right, pos_int_left, radius_int_left, pos_int_right, radius_int_right);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeKeyXY@48")]
        private static extern System.IntPtr MakeKeyXYInternal(double spacing, double radius_ext, double radius_int, double radius, double thickness, double radius_round);

        public static FacePtr MakeKeyXY(double spacing, double radius_ext, double radius_int, double radius, double thickness, double radius_round)
        {
            System.IntPtr temp_object_value = MakeKeyXYInternal(spacing, radius_ext, radius_int, radius, thickness, radius_round);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRodXY@56")]
        private static extern System.IntPtr MakeRodXYInternal(double spacing, double radius_ext_left, double radius_ext_right, double radius_int_left, double radius_int_right, double thickness, double radius_round);

        public static FacePtr MakeRodXY(double spacing, double radius_ext_left, double radius_ext_right, double radius_int_left, double radius_int_right, double thickness, double radius_round)
        {
            System.IntPtr temp_object_value = MakeRodXYInternal(spacing, radius_ext_left, radius_ext_right, radius_int_left, radius_int_right, thickness, radius_round);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakePolygone5FaceXY@80")]
        private static extern System.IntPtr MakePolygone5FaceXYInternal(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y, double p4_x, double p4_y, double p5_x, double p5_y);

        public static FacePtr MakePolygone5FaceXY(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y, double p4_x, double p4_y, double p5_x, double p5_y)
        {
            System.IntPtr temp_object_value = MakePolygone5FaceXYInternal(p1_x, p1_y, p2_x, p2_y, p3_x, p3_y, p4_x, p4_y, p5_x, p5_y);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakePolygone6FaceXY@96")]
        private static extern System.IntPtr MakePolygone6FaceXYInternal(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y, double p4_x, double p4_y, double p5_x, double p5_y, double p6_x, double p6_y);

        public static FacePtr MakePolygone6FaceXY(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y, double p4_x, double p4_y, double p5_x, double p5_y, double p6_x, double p6_y)
        {
            System.IntPtr temp_object_value = MakePolygone6FaceXYInternal(p1_x, p1_y, p2_x, p2_y, p3_x, p3_y, p4_x, p4_y, p5_x, p5_y, p6_x, p6_y);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakePolygone8FaceXY@128")]
        private static extern System.IntPtr MakePolygone8FaceXYInternal(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y, double p4_x, double p4_y, double p5_x, double p5_y, double p6_x, double p6_y, double p7_x, double p7_y, double p8_x, double p8_y);

        public static FacePtr MakePolygone8FaceXY(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y, double p4_x, double p4_y, double p5_x, double p5_y, double p6_x, double p6_y, double p7_x, double p7_y, double p8_x, double p8_y)
        {
            System.IntPtr temp_object_value = MakePolygone8FaceXYInternal(p1_x, p1_y, p2_x, p2_y, p3_x, p3_y, p4_x, p4_y, p5_x, p5_y, p6_x, p6_y, p7_x, p7_y, p8_x, p8_y);
            FacePtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new FacePtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFaceHasIntersections@4")]
        private static extern bool FaceHasIntersectionsInternal(System.IntPtr face);

        public static bool FaceHasIntersections(FacePtr face)
        {
            bool returned_value = FaceHasIntersectionsInternal(face.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewSimpleText@8")]
        private static extern System.IntPtr NewSimpleTextInternal(string text, string font);

        public static TextPtr NewSimpleText(string text, string font)
        {
            System.IntPtr temp_object_value = NewSimpleTextInternal(text, font);
            TextPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new TextPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewTextXY@40")]
        private static extern System.IntPtr NewTextXYInternal(string text, double x, double y, double angle, string font, double font_height);

        public static TextPtr NewTextXY(string text, double x, double y, double angle, string font, double font_height)
        {
            System.IntPtr temp_object_value = NewTextXYInternal(text, x, y, angle, font, font_height);
            TextPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new TextPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteText@4")]
        private static extern void DeleteTextInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dAddTextToFace@8")]
        private static extern void AddTextToFaceInternal(System.IntPtr face, System.IntPtr text);

        public static void AddTextToFace(Face face, TextPtr text)
        {
            AddTextToFaceInternal(face.__Ptr, text.__Ptr);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetTextString@12")]
        private static extern void GetTextStringInternal(System.IntPtr text, System.Text.StringBuilder out_string, int string_size);

        public static void GetTextString(Text text, System.Text.StringBuilder out_string, int string_size)
        {
            GetTextStringInternal(text.__Ptr, out_string, string_size);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTextGetAngle@4")]
        private static extern double TextGetAngleInternal(System.IntPtr text);

        public static double TextGetAngle(Text text)
        {
            double returned_value = TextGetAngleInternal(text.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTextGetHeight@4")]
        private static extern double TextGetHeightInternal(System.IntPtr text);

        public static double TextGetHeight(Text text)
        {
            double returned_value = TextGetHeightInternal(text.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTextGetPoint@12")]
        private static extern void TextGetPointInternal(System.IntPtr text, out double x, out double y);

        public static void TextGetPoint(Text text, out double x, out double y)
        {
            TextGetPointInternal(text.__Ptr, out x, out y);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetTextFont@12")]
        private static extern void GetTextFontInternal(System.IntPtr text, System.Text.StringBuilder out_font_name, int font_name_size);

        public static void GetTextFont(Text text, System.Text.StringBuilder out_font_name, int font_name_size)
        {
            GetTextFontInternal(text.__Ptr, out_font_name, font_name_size);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSetTextHeight@12")]
        private static extern void SetTextHeightInternal(System.IntPtr text, double height);

        public static void SetTextHeight(Text text, double height)
        {
            SetTextHeightInternal(text.__Ptr, height);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSetTextAttachmentType@8")]
        private static extern void SetTextAttachmentTypeInternal(System.IntPtr text, AttachmentType attachment_type);

        public static void SetTextAttachmentType(Text text, AttachmentType attachment_type)
        {
            SetTextAttachmentTypeInternal(text.__Ptr, attachment_type);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dRenderText@4")]
        private static extern System.IntPtr RenderTextInternal(System.IntPtr text);

        public static PartPtr RenderText(Text text)
        {
            System.IntPtr temp_object_value = RenderTextInternal(text.__Ptr);
            PartPtr temp_object = null;
            if (temp_object_value != System.IntPtr.Zero)
            {
                temp_object = new PartPtr();
                temp_object.__Ptr = temp_object_value;
            }
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSetMachiningOnProfile@8")]
        private static extern void SetMachiningOnProfileInternal(System.IntPtr profile, int machining);

        public static void SetMachiningOnProfile(Profile profile, int machining)
        {
            SetMachiningOnProfileInternal(profile.__Ptr, machining);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetMachiningOfProfile@8")]
        private static extern int GetMachiningOfProfileInternal(System.IntPtr profile, out int out_machining);

        public static int GetMachiningOfProfile(Profile profile, out int out_machining)
        {
            int returned_value = GetMachiningOfProfileInternal(profile.__Ptr, out out_machining);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dIsProfileIgnoredByNesting@8")]
        private static extern int IsProfileIgnoredByNestingInternal(System.IntPtr profile, out bool out_nesting_ignored);

        public static int IsProfileIgnoredByNesting(Profile profile, out bool out_nesting_ignored)
        {
            int returned_value = IsProfileIgnoredByNestingInternal(profile.__Ptr, out out_nesting_ignored);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSetMachiningOnText@8")]
        private static extern void SetMachiningOnTextInternal(System.IntPtr text, int machining);

        public static void SetMachiningOnText(Text text, int machining)
        {
            SetMachiningOnTextInternal(text.__Ptr, machining);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetMachiningOfText@8")]
        private static extern int GetMachiningOfTextInternal(System.IntPtr text, out int out_machining);

        public static int GetMachiningOfText(Text text, out int out_machining)
        {
            int returned_value = GetMachiningOfTextInternal(text.__Ptr, out out_machining);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSetBevelAttributeOnEdge@8")]
        private static extern int SetBevelAttributeOnEdgeInternal(System.IntPtr edge, int bevel_type);

        public static int SetBevelAttributeOnEdge(Edge edge, int bevel_type)
        {
            int returned_value = SetBevelAttributeOnEdgeInternal(edge.__Ptr, bevel_type);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dRemoveBevelAttributeOnEdge@4")]
        private static extern void RemoveBevelAttributeOnEdgeInternal(System.IntPtr edge);

        public static void RemoveBevelAttributeOnEdge(Edge edge)
        {
            RemoveBevelAttributeOnEdgeInternal(edge.__Ptr);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSetFieldOfBevelAttributeOnEdge@16")]
        private static extern int SetFieldOfBevelAttributeOnEdgeInternal(System.IntPtr edge, int field, double value);

        public static int SetFieldOfBevelAttributeOnEdge(Edge edge, int field, double value)
        {
            int returned_value = SetFieldOfBevelAttributeOnEdgeInternal(edge.__Ptr, field, value);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetBevelTypeOnEdge@4")]
        private static extern int GetBevelTypeOnEdgeInternal(System.IntPtr edge);

        public static int GetBevelTypeOnEdge(Edge edge)
        {
            int returned_value = GetBevelTypeOnEdgeInternal(edge.__Ptr);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetFieldValueOfBevelAttributeOnEdge@12")]
        private static extern int GetFieldValueOfBevelAttributeOnEdgeInternal(System.IntPtr edge, int field, out double out_value);

        public static int GetFieldValueOfBevelAttributeOnEdge(Edge edge, int field, out double out_value)
        {
            int returned_value = GetFieldValueOfBevelAttributeOnEdgeInternal(edge.__Ptr, field, out out_value);
            return returned_value;
        }

    }
}

#pragma warning restore 169
#pragma warning restore 649
